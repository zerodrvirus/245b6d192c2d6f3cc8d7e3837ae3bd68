-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2018 at 03:26 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vivlio`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `a_id` int(11) NOT NULL,
  `title` text,
  `status` varchar(200) DEFAULT NULL,
  `description` text,
  `ao_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`a_id`, `title`, `status`, `description`, `ao_id`, `created_at`, `updated_at`) VALUES
(15, 'Book Changes', 'active', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2017-08-11 02:56:40', '2017-08-30 03:20:04'),
(71, 'Announcement', 'active', 'rem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occae', 2, '2017-08-14 05:20:07', '2017-08-15 01:28:31'),
(72, 'Book Chang', 'active', 'em ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laboru', 1, '2017-08-14 05:37:50', '2017-08-30 03:20:06'),
(73, 'AA', 'active', 'em ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt m', 1, '2017-08-15 01:13:17', '2017-08-16 02:43:59'),
(74, 'AAAAAA', 'active', 'm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim', 2, '2017-08-15 01:26:29', '2017-08-15 01:28:32'),
(75, 'asd', 'active', 'oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occae', 2, '2017-08-15 01:31:15', '2017-08-15 01:31:15'),
(77, '123123', 'active', 'oris nisi ut aliquip ex ea commodo consequat. Duis aute irure do', 2, '2017-08-15 01:31:31', '2017-08-15 01:31:31');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_option`
--

CREATE TABLE `announcement_option` (
  `ao_id` int(11) UNSIGNED NOT NULL,
  `ao_name` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement_option`
--

INSERT INTO `announcement_option` (`ao_id`, `ao_name`, `created_at`, `updated_at`) VALUES
(1, 'slide', '2017-08-09 00:53:20', '2017-08-09 00:53:22'),
(2, 'running', '2017-08-11 00:54:03', '2017-08-11 01:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `perm_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_role`
--

CREATE TABLE `auth_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_role_permission`
--

CREATE TABLE `auth_role_permission` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `perm_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`user_id`, `username`, `password`, `salt`, `remember_token`, `created_at`, `updated_at`, `status_id`) VALUES
(1, 'tomskie1995', '$2y$10$eQx7X0ShiXpIfeucNlP9ieJBGzPty6Rvzxp84n00wGGtnJbYhrl2K', '$2y$10$1S6C5u.DC/zjhaTz4QUgqel/WaVKjsIomD81tPPDoA8xpscwqlqri', 'CVAL74uMHlseu2ycwB6o48wLpIsYLqD0mgf1bd0NIJPfQKaq4rteu2o8FfoF', '2017-05-10 01:15:49', '2017-05-22 01:15:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_role`
--

CREATE TABLE `auth_user_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `catalogue_record`
--

CREATE TABLE `catalogue_record` (
  `catalogue_id` int(10) UNSIGNED NOT NULL,
  `material_type_id` int(10) UNSIGNED DEFAULT NULL,
  `call_num` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `remarks` text CHARACTER SET latin1,
  `price` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catalogue_record`
--

INSERT INTO `catalogue_record` (`catalogue_id`, `material_type_id`, `call_num`, `remarks`, `price`, `created_at`, `updated_at`) VALUES
(16, 1, '005.115 f2471', '\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, '2017-11-07 05:11:52', '2017-11-07 05:11:52'),
(17, 1, NULL, '\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, '2017-11-07 05:11:51', '2017-11-07 05:11:51'),
(18, 1, NULL, '\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia ', NULL, '2017-11-07 05:52:18', '2017-11-07 05:52:18'),
(19, 4, '12345', '\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '10', '2017-11-07 05:11:51', '2017-11-07 05:11:51'),
(20, 1, 'dfg', 'dfg', 'dfgdf', '2017-11-13 01:28:53', '2017-11-13 01:28:53'),
(23, NULL, NULL, NULL, NULL, '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(24, NULL, NULL, NULL, NULL, '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(25, NULL, NULL, NULL, NULL, '2018-05-09 02:38:34', '2018-05-09 02:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `category_name` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Library Logs', '2017-06-27 02:16:40', '2017-06-27 02:16:40'),
(2, 'Multimedia Logs', '2017-06-27 02:16:43', '2017-06-27 02:16:43'),
(3, 'Internet Logs', '2017-06-27 02:16:45', '2017-06-27 02:16:45');

-- --------------------------------------------------------

--
-- Table structure for table `cat_templates`
--

CREATE TABLE `cat_templates` (
  `template_id` int(11) UNSIGNED NOT NULL,
  `template_name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_templates`
--

INSERT INTO `cat_templates` (`template_id`, `template_name`, `description`) VALUES
(8, 'quick add', 'basic cataloging'),
(9, 'book', 'monograph'),
(10, 'Serials', 'Journal, Newspaper, Periodical');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `c_id` int(11) NOT NULL,
  `c_name` text,
  `c_description` text,
  `c_TIN` varchar(200) DEFAULT NULL,
  `c_postal` varchar(200) DEFAULT NULL,
  `c_contact` varchar(200) DEFAULT NULL,
  `c_email` varchar(200) DEFAULT NULL,
  `c_status` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`c_id`, `c_name`, `c_description`, `c_TIN`, `c_postal`, `c_contact`, `c_email`, `c_status`, `created_at`, `updated_at`) VALUES
(1, 'ACLC College Of Butuan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '214-1293-2828-12011', '8600', '0912837200', 'aclcbutuan123@gmail.com', 'active', '2017-08-15 02:54:04', '2017-08-17 02:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `copies`
--

CREATE TABLE `copies` (
  `copy_id` int(10) UNSIGNED NOT NULL,
  `acc_num` varchar(200) CHARACTER SET latin1 NOT NULL,
  `catalogue_id` int(10) UNSIGNED DEFAULT NULL,
  `barcode` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `source` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `status` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `call_num` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `issues` smallint(6) DEFAULT NULL,
  `date_received` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `price` double(10,0) DEFAULT NULL,
  `material_type_id` int(10) DEFAULT NULL,
  `copy_number` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copies`
--

INSERT INTO `copies` (`copy_id`, `acc_num`, `catalogue_id`, `barcode`, `source`, `status`, `note`, `call_num`, `issues`, `date_received`, `created_at`, `updated_at`, `price`, `material_type_id`, `copy_number`) VALUES
(1, '0001653-12', 17, '123456789', 'donation', 'available', 'this is a note', '005.2 c263 2009', NULL, '2017-08-29', '2017-09-11 02:52:46', '2018-02-05 07:25:11', NULL, NULL, NULL),
(2, '0002314-16', 18, '2', 'donation', 'available', 'In publishing and graphic design, lorem ipsum is a filler text or greeking commonly used to demonstrate the textual elements of a graphic document or visual presentation. Replacing meaningful content with placeholder text allows designers to design the form of the content before the content itself has been produced.', '005.131 r4495 2014', NULL, '2017-08-30', '2017-09-11 02:52:46', '2018-05-09 02:50:54', NULL, NULL, NULL),
(3, '1', 19, '123456', 'f', 'available', 'In publishing and graphic design, lorem ipsum is a filler text or greeking commonly used to demonstrate the textual elements of a graphic document or visual presentation. Replacing meaningful content with placeholder text allows designers to design the form of the content before the content itself has been produced.', '5', NULL, '2017-09-11', '2017-09-11 02:52:46', '2018-04-16 02:30:05', NULL, NULL, NULL),
(4, '100199272', 16, '3', 'donation', 'available', 'In publishing and graphic design, lorem ipsum is a filler text or greeking commonly used to demonstrate the textual elements of a graphic document or visual presentation. Replacing meaningful content with placeholder text allows designers to design the form of the content before the content itself has been produced.', '005.115 F2471 2015', NULL, '2017-08-24', '2017-09-11 02:52:46', '2018-05-09 02:52:56', NULL, NULL, NULL),
(5, '2', 17, '12345', 'set', 'available', 'In publishing and graphic design, lorem ipsum is a filler text or greeking commonly used to demonstrate the textual elements of a graphic document or visual presentation. Replacing meaningful content with placeholder text allows designers to design the form of the content before the content itself has been produced.', '12345', NULL, '2017-12-12', '2017-12-11 16:00:00', '2018-05-09 02:50:52', NULL, NULL, NULL),
(6, '12331233', 20, '112233', 'donation', 'available', 'In publishing and graphic design, lorem ipsum is a filler text or greeking commonly used to demonstrate the textual elements of a graphic document or visual presentation. Replacing meaningful content with placeholder text allows designers to design the form of the content before the content itself has been produced.', '23F', NULL, '2017-12-12', '2018-12-02 03:07:20', '2018-02-06 01:30:29', NULL, NULL, NULL),
(7, '31323', 25, '789', 'SOURCE_1', 'available', 'SOURCE SYSIOO', '1253', NULL, '2018-05-09', '2018-05-09 02:40:08', '2018-05-09 02:40:08', 300, 2, 3335767);

-- --------------------------------------------------------

--
-- Table structure for table `expiration_history`
--

CREATE TABLE `expiration_history` (
  `expiration_id` int(10) UNSIGNED NOT NULL,
  `patron_id` int(10) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expiration_history`
--

INSERT INTO `expiration_history` (`expiration_id`, `patron_id`, `expiration_date`, `created_at`, `updated_at`) VALUES
(28, 188, '2017-10-29 13:48:12', '2017-07-10 05:48:13', '2017-07-10 05:48:13'),
(29, 189, '2017-10-29 13:52:38', '2017-07-10 05:52:38', '2017-07-10 05:52:38'),
(30, 190, '2017-10-29 13:54:17', '2017-07-10 05:54:17', '2017-07-10 05:54:17'),
(44, 204, '2017-10-29 14:55:41', '2017-07-11 06:55:41', '2017-07-11 06:55:41'),
(45, 205, '2017-10-29 15:07:15', '2017-07-11 07:07:15', '2017-07-11 07:07:15'),
(46, 206, '2017-10-29 15:08:51', '2017-07-11 07:08:51', '2017-07-11 07:08:51'),
(52, 212, '2017-10-29 08:25:02', '2017-07-13 00:25:02', '2017-07-13 00:25:02'),
(53, 213, '2017-10-29 08:26:30', '2017-07-13 00:26:30', '2017-07-13 00:26:30'),
(54, 214, '2017-10-29 08:26:32', '2017-07-13 00:26:32', '2017-07-13 00:26:32'),
(55, 215, '2017-10-29 08:26:34', '2017-07-13 00:26:35', '2017-07-13 00:26:35'),
(56, 188, '2017-10-29 16:40:41', '2017-09-04 01:51:22', '2017-09-04 01:51:22'),
(57, 216, '2017-10-29 08:57:22', '2017-08-07 00:57:22', '2017-08-07 00:57:22'),
(58, 217, '2017-10-29 15:17:26', '2017-08-15 07:17:26', '2017-08-15 07:17:26'),
(59, 218, '2017-10-29 15:17:28', '2017-08-15 07:17:28', '2017-08-15 07:17:28'),
(60, 219, '2017-10-29 15:17:31', '2017-08-15 07:17:31', '2017-08-15 07:17:31'),
(61, 220, '2017-10-29 15:17:33', '2017-08-15 07:17:34', '2017-08-15 07:17:34'),
(62, 221, '2017-10-29 15:17:37', '2017-08-15 07:17:38', '2017-08-15 07:17:38'),
(63, 222, '2017-10-29 15:17:40', '2017-08-15 07:17:40', '2017-08-15 07:17:40'),
(64, 223, '2017-10-29 15:17:44', '2017-08-15 07:17:44', '2017-08-15 07:17:44'),
(65, 224, '2017-10-29 15:17:46', '2017-08-15 07:17:46', '2017-08-15 07:17:46'),
(66, 225, '2017-10-29 15:17:48', '2017-08-15 07:17:48', '2017-08-15 07:17:48'),
(67, 226, '2017-10-29 15:17:53', '2017-08-15 07:17:53', '2017-08-15 07:17:53'),
(68, 227, '2017-10-29 15:17:54', '2017-08-15 07:17:54', '2017-08-15 07:17:54'),
(69, 228, '2017-10-29 15:17:56', '2017-08-15 07:17:56', '2017-08-15 07:17:56'),
(70, 188, '2017-10-29 09:37:14', '2017-09-04 01:51:20', '2017-09-04 01:51:20'),
(71, 188, '2017-10-29 09:46:54', '2017-09-04 01:51:20', '2017-09-04 01:51:20'),
(72, 188, '2017-10-29 09:48:06', '2017-09-04 01:51:20', '2017-09-04 01:51:20'),
(73, 188, '2017-10-29 09:51:10', '2017-09-04 01:51:10', '2017-09-04 01:51:10'),
(74, 229, '2017-10-29 16:58:46', '2017-09-06 08:58:46', '2017-09-06 08:58:46'),
(75, 230, '2017-10-29 10:11:04', '2017-09-11 02:11:04', '2017-09-11 02:11:04'),
(76, 189, '2017-10-29 09:01:10', '2017-09-12 01:01:10', '2017-09-12 01:01:10'),
(77, 231, '2017-10-29 14:25:52', '2017-09-12 06:25:52', '2017-09-12 06:25:52'),
(78, 232, '2017-10-29 10:26:23', '2017-09-26 02:26:24', '2017-09-26 02:26:24'),
(79, 189, '2017-10-29 08:58:37', '2017-11-02 00:58:37', '2017-11-02 00:58:37'),
(80, 213, '2017-10-29 13:34:41', '2017-11-02 05:34:41', '2017-11-02 05:34:41'),
(81, 205, '2017-10-29 14:53:14', '2017-11-08 06:53:14', '2017-11-08 06:53:14'),
(82, 231, '2017-10-29 14:25:52', '2017-09-12 06:25:52', '2017-09-12 06:25:52'),
(83, 232, '2017-10-29 10:26:23', '2017-09-26 02:26:24', '2017-09-26 02:26:24'),
(84, 189, '2017-10-29 08:58:37', '2017-11-02 00:58:37', '2017-11-02 00:58:37'),
(85, 213, '2017-10-29 13:34:41', '2017-11-02 05:34:41', '2017-11-02 05:34:41'),
(86, 205, '2017-10-29 14:53:14', '2017-11-08 06:53:14', '2017-11-08 06:53:14'),
(87, 231, '2017-10-29 14:25:52', '2017-09-12 06:25:52', '2017-09-12 06:25:52'),
(88, 232, '2017-10-29 10:26:23', '2017-09-26 02:26:24', '2017-09-26 02:26:24'),
(89, 189, '2017-10-29 08:58:37', '2017-11-02 00:58:37', '2017-11-02 00:58:37'),
(90, 213, '2017-10-29 13:34:41', '2017-11-02 05:34:41', '2017-11-02 05:34:41'),
(91, 205, '2017-10-29 14:53:14', '2017-11-08 06:53:14', '2017-11-08 06:53:14'),
(92, 204, '2018-10-29 13:09:25', '2018-01-16 05:09:25', '2018-01-16 05:09:25'),
(93, 189, '2018-10-29 10:28:59', '2018-04-16 02:29:00', '2018-04-16 02:29:00'),
(94, 233, NULL, '2018-05-09 02:51:54', '2018-05-09 02:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `e_resources`
--

CREATE TABLE `e_resources` (
  `res_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_resources`
--

INSERT INTO `e_resources` (`res_id`, `name`, `edition`, `type`, `description`, `created_at`, `updated_at`) VALUES
(30, 'CHAPTER 3', 'CHAPTER #', 'docx', 'CHAPTER FOR UNITY', '2018-02-08 02:25:41', '2018-02-08 02:25:41'),
(51, 'TIMELINE', 'TIME-LINE', 'xlsx', 'TIMELINE', '2018-02-09 05:43:56', '2018-02-09 05:43:56'),
(52, 'TRANSMISSION', '2nd EDITION', 'pdf', 'sample', '2018-02-09 05:52:58', '2018-02-09 05:52:58'),
(53, 'POWER POINT', 'PPT', 'pptx', '   Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate fuga accusantium laboriosam doloribus voluptates, est placeat repudiandae. Aut placeat delectus necessitatibus reprehenderit quis maiores magni, velit nam, quisquam, obcaecati rem?\r\n				     	 	 ', '2018-02-09 06:01:12', '2018-02-09 06:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `field_value`
--

CREATE TABLE `field_value` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED DEFAULT NULL,
  `catalogue_id` int(10) UNSIGNED DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `field_value`
--

INSERT INTO `field_value` (`field_id`, `id`, `catalogue_id`, `value`, `created_at`, `updated_at`) VALUES
(51, 14, 16, '_a978-1-285-84577-7', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(52, 15, 16, '_ajoyce farrell_d2015', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(53, 16, 16, '_aprogramming logic and design_bintroductory version_c', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(54, 17, 16, '_aeighth', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(55, 18, 16, '_a200 first stamford place, 4th floor stamford, CT 06902_bcengage learning_c2015', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(56, 19, 16, '_a355_b_c', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(57, 20, 16, '_v', '2017-08-23 07:57:26', '2017-08-23 07:57:26'),
(58, 14, 17, '_a2008019142', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(59, 15, 17, '_ahenri casanova_d', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(60, 16, 17, '_aparallel algorithms_bnumerical analysis and scientific computing_c', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(61, 17, 17, '_a', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(62, 18, 17, '_a4th, floor, albert house, 1-4 singer street london ec2a 4bq, uk_bcrc press, taylor and francis group_c', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(63, 19, 17, '_a_b_c', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(64, 20, 17, '_v', '2017-08-29 06:55:07', '2017-08-29 06:55:07'),
(65, 14, 18, '_a978-1-8421-615-0', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(66, 15, 18, '_amichel rigo_d', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(67, 16, 18, '_aformal languages, automata and numeration systems 1_bintroduction to combination n words_cnetworks and telecommunications series', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(68, 17, 18, '_a', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(69, 18, 18, '_alondon_biste ltd_c', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(70, 19, 18, '_a303_b_c', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(71, 20, 18, '_v2', '2017-08-30 07:59:23', '2017-08-30 07:59:23'),
(72, 14, 19, '_aasd', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(73, 15, 19, '_ajyde_djyde2017', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(74, 16, 19, '_azzzzReow_bRemainder_cRemainder', '2017-09-11 02:48:34', '2017-11-06 06:40:27'),
(75, 17, 19, '_a1', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(76, 18, 19, '_abutuan_bme_c2017', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(77, 19, 19, '_aa_b4x4_ca', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(78, 20, 19, '_v1', '2017-09-11 02:48:34', '2017-09-11 02:48:34'),
(79, 14, 20, '_adfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(80, 15, 20, '_adfg_ddf', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(81, 16, 20, '_adfg_bdfg_cdfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(82, 17, 20, '_adfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(83, 18, 20, '_adfg_bdfg_cdfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(84, 19, 20, '_adfg_bdfg_cdfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(85, 20, 20, '_vdfg', '2017-11-13 01:28:54', '2017-11-13 01:28:54'),
(86, 14, 23, '_a889977-9668_c_z', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(87, 15, 23, '_a_b_c_q_d', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(88, 16, 23, '_a_p_b_c', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(89, 17, 23, '_a', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(90, 18, 23, '_a_b_c', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(91, 19, 23, '_a_b_c_e', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(92, 20, 23, '_a_v', '2018-05-09 01:59:04', '2018-05-09 01:59:04'),
(93, 14, 24, '_a300-1029293-213-222_c3_z', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(94, 15, 24, '_a_b_c_q_d', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(95, 16, 24, '_a_p_b_c', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(96, 17, 24, '_a3rd Edition', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(97, 18, 24, '_a_b_c', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(98, 19, 24, '_a_b_c_e', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(99, 20, 24, '_a_v', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(100, 21, 24, '_a_c_D', '2018-05-09 02:34:54', '2018-05-09 02:34:54'),
(101, 22, 24, '_a_n_d_c', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(102, 23, 24, '_a_p_l_s_f', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(103, 24, 24, '_a_l_f', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(104, 25, 24, '_a', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(105, 26, 24, '_a_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(106, 27, 24, '_a_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(107, 28, 24, '_a_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(108, 29, 24, '_aSystem Out Print _v3', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(109, 30, 24, '_a', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(110, 31, 24, '_a332', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(111, 32, 24, '_a', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(112, 33, 24, '_a', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(113, 34, 24, '_a_B', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(114, 35, 24, '_a', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(115, 36, 24, '_P_C_T', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(116, 37, 24, '_ATom Ramos Pedales _B300_CTommy Society _fuller Clear _D_TWork Of Reich_V2014_XYouth_YChrono_zPhilippines_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(117, 38, 24, '_A_B_V_X_Y_Z_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(118, 39, 24, '_A_V_X_Y_Z_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(119, 40, 24, '_APhillipines_V_X_Y_Z_2', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(120, 41, 24, '_A_b_C_Q_d_E_4', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(121, 42, 24, '_A_B', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(122, 43, 24, '_ATommy_B_C_Q_D_T_V', '2018-05-09 02:34:55', '2018-05-09 02:34:55'),
(123, 14, 25, '_a300012_cSystem Out Print_z', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(124, 15, 25, '_a_b_c_q_d', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(125, 16, 25, '_aSystem Out Print _p_b_c', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(126, 17, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(127, 18, 25, '_a_b_c', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(128, 19, 25, '_a_b_c_e', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(129, 20, 25, '_a_v', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(130, 21, 25, '_a_c_D', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(131, 22, 25, '_a_n_d_c', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(132, 23, 25, '_a_p_l_s_f', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(133, 24, 25, '_a_l_f', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(134, 25, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(135, 26, 25, '_a_2', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(136, 27, 25, '_a_2', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(137, 28, 25, '_a_2', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(138, 29, 25, '_a_v', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(139, 30, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(140, 31, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(141, 32, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(142, 33, 25, '_a', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(143, 34, 25, '_a_B', '2018-05-09 02:38:34', '2018-05-09 02:38:34'),
(144, 35, 25, '_a', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(145, 36, 25, '_P_C_T', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(146, 37, 25, '_A_B_C_Q_D_T_V_X_Y_z_2', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(147, 38, 25, '_A_B_V_X_Y_Z_2', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(148, 39, 25, '_A_V_X_Y_Z_2', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(149, 40, 25, '_A_V_X_Y_Z_2', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(150, 41, 25, '_A_b_C_Q_d_E_4', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(151, 42, 25, '_A_B', '2018-05-09 02:38:35', '2018-05-09 02:38:35'),
(152, 43, 25, '_ATom _B33123_C_Q_D_T_V', '2018-05-09 02:38:35', '2018-05-09 02:38:35');

-- --------------------------------------------------------

--
-- Table structure for table `fines`
--

CREATE TABLE `fines` (
  `f_id` int(10) UNSIGNED NOT NULL,
  `patron_id` int(11) DEFAULT NULL,
  `loan_id` int(11) UNSIGNED DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `remarks` text,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fines`
--

INSERT INTO `fines` (`f_id`, `patron_id`, `loan_id`, `amount`, `type`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(44, 189, 176, '20.00', 'CASH', 'late kaayu ni cya', 'PAY', '2018-04-16 02:47:38', '2018-04-16 02:47:38'),
(45, 189, 176, '20.00', 'CASH', 'late ni cya', 'PAY', '2018-04-16 02:48:38', '2018-04-16 02:48:38'),
(46, 189, 181, '480.00', 'CASH', '333', 'PAY', '2018-05-09 02:48:33', '2018-05-09 02:48:33'),
(47, 189, 182, '380.00', 'CASH', '2222', 'PAY', '2018-05-09 02:48:33', '2018-05-09 02:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `lib_material_type`
--

CREATE TABLE `lib_material_type` (
  `material_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lib_material_type`
--

INSERT INTO `lib_material_type` (`material_type_id`, `name`, `description`) VALUES
(1, 'photograph, print or drawing', NULL),
(2, 'rare book or manuscript', NULL),
(3, 'map', NULL),
(4, 'music and nonmusic recording', NULL),
(5, 'software or e-resource', NULL),
(6, 'film or video', NULL),
(7, 'periodical or newspaper', NULL),
(8, 'all text', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `loan_id` int(10) UNSIGNED NOT NULL,
  `patron_id` int(10) NOT NULL,
  `acc_num` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `returned` varchar(4) CHARACTER SET latin1 DEFAULT NULL,
  `returned_date` datetime DEFAULT NULL,
  `loaned_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`loan_id`, `patron_id`, `acc_num`, `due_date`, `returned`, `returned_date`, `loaned_date`, `created_at`, `updated_at`) VALUES
(174, 189, '0002314-16', '2018-04-19 10:32:09', '1', '2018-04-16 10:32:20', '2018-04-16 10:32:09', '2018-04-16 02:32:09', '2018-04-16 02:32:20'),
(175, 189, '100199272', '2018-04-19 10:32:09', '1', '2018-04-16 10:49:06', '2018-04-16 10:32:09', '2018-04-16 02:32:09', '2018-04-16 02:49:06'),
(176, 189, '2', '2018-04-15 10:32:09', '1', '2018-04-16 10:48:38', '2018-04-16 10:32:09', '2018-04-16 02:32:09', '2018-04-16 02:48:38'),
(177, 189, '0002314-16', '2018-04-19 10:49:00', '1', '2018-04-16 10:49:08', '2018-04-16 10:49:00', '2018-04-16 02:49:00', '2018-04-16 02:49:08'),
(178, 189, '0002314-16', '2018-04-19 10:49:16', '1', '2018-04-16 10:50:09', '2018-04-16 10:49:16', '2018-04-16 02:49:16', '2018-04-16 02:50:09'),
(179, 189, '100199272', '2018-04-19 10:49:17', '1', '2018-04-16 10:50:11', '2018-04-16 10:49:17', '2018-04-16 02:49:17', '2018-04-16 02:50:11'),
(180, 189, '2', '2018-04-19 10:49:18', '1', '2018-04-16 10:50:14', '2018-04-16 10:49:18', '2018-04-16 02:49:18', '2018-04-16 02:50:14'),
(181, 189, '0002314-16', '2018-04-14 10:51:20', '1', '2018-05-09 10:48:33', '2018-04-16 10:51:20', '2018-04-16 02:51:20', '2018-05-09 02:48:33'),
(182, 189, '100199272', '2018-04-19 10:51:20', '1', '2018-05-09 10:48:33', '2018-04-16 10:51:20', '2018-04-16 02:51:20', '2018-05-09 02:48:33'),
(183, 204, '2', '2018-05-11 11:03:49', '1', '2018-05-08 11:04:15', '2018-05-08 11:03:49', '2018-05-08 03:03:49', '2018-05-08 03:04:15'),
(184, 189, '2', '2018-05-11 11:12:01', '1', '2018-05-08 11:12:14', '2018-05-08 11:12:01', '2018-05-08 03:12:01', '2018-05-08 03:12:14'),
(185, 189, '2', '2018-05-12 10:49:40', '1', '2018-05-09 10:50:02', '2018-05-09 10:49:40', '2018-05-09 02:49:40', '2018-05-09 02:50:02'),
(186, 189, '100199272', '2018-05-12 10:49:41', '1', '2018-05-09 10:50:55', '2018-05-09 10:49:41', '2018-05-09 02:49:41', '2018-05-09 02:50:55'),
(187, 189, '0002314-16', '2018-05-12 10:50:11', '1', '2018-05-09 10:50:54', '2018-05-09 10:50:11', '2018-05-09 02:50:11', '2018-05-09 02:50:54'),
(188, 189, '2', '2018-05-12 10:50:39', '1', '2018-05-09 10:50:52', '2018-05-09 10:50:39', '2018-05-09 02:50:39', '2018-05-09 02:50:52'),
(189, 233, '100199272', '2018-05-12 10:52:24', '1', '2018-05-09 10:52:38', '2018-05-09 10:52:24', '2018-05-09 02:52:24', '2018-05-09 02:52:38'),
(190, 233, '100199272', '2018-05-12 10:52:45', '1', '2018-05-09 10:52:56', '2018-05-09 10:52:45', '2018-05-09 02:52:45', '2018-05-09 02:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `loan_rules`
--

CREATE TABLE `loan_rules` (
  `patron_category_id` int(10) UNSIGNED NOT NULL,
  `fine` decimal(10,0) DEFAULT NULL,
  `max_loan_qty` int(5) DEFAULT NULL,
  `loan_length` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loan_rules`
--

INSERT INTO `loan_rules` (`patron_category_id`, `fine`, `max_loan_qty`, `loan_length`) VALUES
(1, '20', 3, 3),
(2, '43', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL,
  `patron_id` int(11) DEFAULT NULL,
  `patron_ids` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `patron_id`, `patron_ids`, `cat_id`, `created_at`, `updated_at`) VALUES
(117, NULL, 186, 3, '2017-06-22 10:14:17', '2017-06-29 02:53:04'),
(118, NULL, 186, 2, '2017-06-27 10:13:55', '2017-06-27 10:13:55'),
(119, NULL, 186, 2, '2017-06-27 10:14:11', '2017-06-27 10:14:11'),
(120, NULL, 186, 2, '2017-06-27 10:14:13', '2017-06-27 10:14:13'),
(121, NULL, 186, 2, '2017-06-27 10:14:14', '2017-06-27 10:14:14'),
(122, NULL, 186, 2, '2017-06-27 10:14:15', '2017-06-27 10:14:15'),
(123, NULL, 186, 2, '2017-06-22 10:14:17', '2017-06-29 02:52:59'),
(124, NULL, 186, 2, '2017-06-22 10:14:17', '2017-06-29 02:52:56'),
(125, NULL, 186, 2, '2017-06-27 10:14:17', '2017-06-27 10:14:17'),
(126, NULL, 186, 2, '2017-06-22 10:14:17', '2017-06-29 02:53:02'),
(127, NULL, 186, 2, '2017-06-27 10:14:19', '2017-06-27 10:14:19'),
(128, NULL, 186, 2, '2017-06-27 10:14:19', '2017-06-27 10:14:19'),
(129, NULL, 186, 2, '2017-06-27 10:14:20', '2017-06-27 10:14:20'),
(130, NULL, 186, 2, '2017-06-27 10:14:21', '2017-06-27 10:14:21'),
(131, NULL, 186, 1, '2017-06-27 10:14:53', '2017-06-27 10:14:53'),
(132, NULL, 186, 1, '2017-06-22 10:14:17', '2017-06-29 02:53:07'),
(133, NULL, 186, 1, '2017-06-23 10:14:17', '2017-06-29 02:53:22'),
(134, NULL, 186, 1, '2017-06-27 10:14:56', '2017-06-27 10:14:56'),
(135, NULL, 186, 1, '2017-06-27 10:14:57', '2017-06-27 10:14:57'),
(136, NULL, 186, 2, '2017-06-29 03:00:14', '2017-06-29 03:00:14'),
(137, NULL, 185, 2, '2017-06-29 03:00:16', '2017-06-29 03:00:16'),
(138, NULL, 185, 1, '2017-06-29 02:52:28', '2017-06-29 02:52:28'),
(139, NULL, 185, 1, '2017-06-29 02:53:36', '2017-06-29 02:53:36'),
(140, NULL, 185, 1, '2017-06-29 02:53:37', '2017-06-29 02:53:37'),
(141, NULL, 185, 1, '2017-06-29 02:53:38', '2017-06-29 02:53:38'),
(142, NULL, 185, 1, '2017-06-29 02:53:38', '2017-06-29 02:53:38'),
(143, NULL, 185, 1, '2017-06-29 02:53:39', '2017-06-29 02:53:39'),
(144, NULL, 185, 2, '2017-06-29 02:53:46', '2017-06-29 02:53:46'),
(145, NULL, 185, 2, '2017-06-29 02:53:47', '2017-06-29 02:53:47'),
(146, NULL, 185, 2, '2017-06-29 02:53:48', '2017-06-29 02:53:48'),
(147, NULL, 185, 2, '2017-06-29 02:53:49', '2017-06-29 02:53:49'),
(148, NULL, 185, 3, '2017-06-29 03:00:19', '2017-06-29 03:00:19'),
(149, NULL, 185, 3, '2017-06-29 02:54:07', '2017-06-29 02:54:07'),
(150, NULL, 185, 1, '2017-06-29 03:00:21', '2017-06-29 03:00:21'),
(151, NULL, 185, 3, '2017-06-28 02:54:09', '2017-06-29 02:54:38'),
(152, NULL, 185, 2, '2017-06-29 03:00:24', '2017-06-29 03:00:24'),
(153, NULL, 185, 1, '2017-06-29 03:02:35', '2017-06-29 03:02:35'),
(154, NULL, 185, 1, '2017-06-29 03:02:36', '2017-06-29 03:02:36'),
(155, NULL, 185, 1, '2017-06-29 03:02:37', '2017-06-29 03:02:37'),
(156, NULL, 185, 1, '2017-06-29 03:02:38', '2017-06-29 03:02:38'),
(157, NULL, 185, 1, '2017-06-29 03:02:39', '2017-06-29 03:02:39'),
(158, NULL, 185, 1, '2017-06-29 03:02:40', '2017-06-29 03:02:40'),
(159, NULL, 185, 1, '2017-06-29 03:02:41', '2017-06-29 03:02:41'),
(160, NULL, 185, 1, '2017-06-29 03:02:42', '2017-06-29 03:02:42'),
(161, NULL, 185, 1, '2017-06-29 03:02:59', '2017-06-29 03:02:59'),
(162, NULL, 185, 1, '2017-06-29 03:03:00', '2017-06-29 03:03:00'),
(163, NULL, 185, 1, '2017-06-29 03:03:01', '2017-06-29 03:03:01'),
(164, NULL, 185, 1, '2017-06-29 03:03:02', '2017-06-29 03:03:02'),
(165, NULL, 185, 1, '2017-06-29 03:03:02', '2017-06-29 03:03:02'),
(166, NULL, 185, 1, '2017-06-29 03:03:03', '2017-06-29 03:03:03'),
(167, NULL, 185, 1, '2017-06-29 03:03:04', '2017-06-29 03:03:04'),
(168, NULL, 185, 1, '2017-06-29 03:03:04', '2017-06-29 03:03:04'),
(169, NULL, 185, 1, '2017-06-29 03:03:05', '2017-06-29 03:03:05'),
(170, NULL, 185, 1, '2017-06-29 03:03:06', '2017-06-29 03:03:06'),
(171, NULL, 185, 1, '2017-06-29 03:03:06', '2017-06-29 03:03:06'),
(172, NULL, 185, 1, '2017-06-29 03:03:07', '2017-06-29 03:03:07'),
(173, NULL, 185, 1, '2017-06-29 03:03:07', '2017-06-29 03:03:07'),
(174, NULL, 185, 1, '2017-06-29 03:03:08', '2017-06-29 03:03:08'),
(175, NULL, 185, 1, '2017-06-29 03:03:09', '2017-06-29 03:03:09'),
(176, NULL, 185, 1, '2017-06-29 03:03:09', '2017-06-29 03:03:09'),
(177, NULL, 185, 1, '2017-06-29 03:03:10', '2017-06-29 03:03:10'),
(178, NULL, 185, 1, '2017-06-29 03:03:11', '2017-06-29 03:03:11'),
(179, NULL, 185, 1, '2017-06-29 03:03:12', '2017-06-29 03:03:12'),
(180, NULL, 185, 1, '2017-06-29 03:03:12', '2017-06-29 03:03:12'),
(181, NULL, 185, 1, '2017-06-29 03:03:13', '2017-06-29 03:03:13'),
(182, NULL, 185, 1, '2017-06-29 03:04:07', '2017-06-29 03:04:07'),
(183, NULL, 185, 1, '2017-06-29 03:04:07', '2017-06-29 03:04:07'),
(184, NULL, 185, 1, '2017-06-29 03:04:07', '2017-06-29 03:04:07'),
(185, NULL, 185, 2, '2017-06-29 03:04:16', '2017-06-29 03:04:16'),
(186, NULL, 185, 2, '2017-06-29 03:04:16', '2017-06-29 03:04:16'),
(187, NULL, 185, 2, '2017-06-29 03:04:17', '2017-06-29 03:04:17'),
(188, NULL, 185, 2, '2017-06-29 03:04:17', '2017-06-29 03:04:17'),
(189, NULL, 185, 2, '2017-06-29 03:04:18', '2017-06-29 03:04:18'),
(190, NULL, 185, 2, '2017-06-29 03:04:19', '2017-06-29 03:04:19'),
(191, NULL, 185, 2, '2017-06-29 03:04:19', '2017-06-29 03:04:19'),
(192, NULL, 185, 2, '2017-06-29 03:04:20', '2017-06-29 03:04:20'),
(193, NULL, 185, 2, '2017-06-29 03:04:21', '2017-06-29 03:04:21'),
(194, NULL, 185, 2, '2017-06-29 03:04:22', '2017-06-29 03:04:22'),
(195, NULL, 185, 2, '2017-06-29 03:04:23', '2017-06-29 03:04:23'),
(196, NULL, 185, 2, '2017-06-29 03:04:23', '2017-06-29 03:04:23'),
(197, NULL, 185, 2, '2017-06-29 03:04:24', '2017-06-29 03:04:24'),
(198, NULL, 185, 2, '2017-06-29 03:04:25', '2017-06-29 03:04:25'),
(199, NULL, 185, 2, '2017-06-29 03:04:25', '2017-06-29 03:04:25'),
(200, NULL, 185, 2, '2017-06-29 03:04:26', '2017-06-29 03:04:26'),
(201, NULL, 185, 2, '2017-06-29 03:04:26', '2017-06-29 03:04:26'),
(202, NULL, 185, 2, '2017-06-29 03:04:27', '2017-06-29 03:04:27'),
(203, 188, 188, 2, '2017-07-21 03:08:18', '2017-07-21 03:08:18'),
(204, 188, 188, 2, '2017-07-21 03:08:22', '2017-07-21 03:08:22'),
(205, 216, 216, 1, '2017-08-15 07:25:33', '2017-08-15 07:25:33'),
(206, 216, 216, 1, '2017-08-15 07:25:34', '2017-08-15 07:25:34'),
(207, 216, 216, 1, '2017-08-15 07:25:35', '2017-08-15 07:25:35'),
(208, 216, 216, 1, '2017-08-15 07:25:36', '2017-08-15 07:25:36'),
(209, 216, 216, 2, '2017-08-15 07:26:10', '2017-08-15 07:26:10'),
(210, 216, 216, 2, '2017-08-15 07:26:11', '2017-08-15 07:26:11'),
(211, 216, 216, 2, '2017-08-15 07:26:13', '2017-08-15 07:26:13'),
(212, 216, 216, 2, '2017-08-15 07:26:14', '2017-08-15 07:26:14'),
(213, 216, 216, 2, '2017-08-15 07:26:15', '2017-08-15 07:26:15'),
(214, 216, 216, 2, '2017-08-15 07:26:16', '2017-08-15 07:26:16'),
(215, 216, 216, 2, '2017-08-15 07:26:16', '2017-08-15 07:26:16'),
(216, 216, 216, 2, '2017-08-15 07:26:17', '2017-08-15 07:26:17'),
(217, 216, 216, 2, '2017-08-15 07:26:18', '2017-08-15 07:26:18'),
(218, 216, 216, 2, '2017-08-15 07:26:19', '2017-08-15 07:26:19'),
(219, 216, 216, 2, '2017-08-15 07:26:20', '2017-08-15 07:26:20'),
(220, 217, 217, 2, '2017-08-15 07:27:04', '2017-08-15 07:27:04'),
(221, 217, 217, 2, '2017-08-15 07:27:06', '2017-08-15 07:27:06'),
(222, 217, 217, 2, '2017-08-15 07:27:08', '2017-08-15 07:27:08'),
(223, 217, 217, 3, '2017-08-15 07:27:52', '2017-08-15 07:27:52'),
(224, 217, 217, 3, '2017-08-15 07:27:53', '2017-08-15 07:27:53'),
(225, 217, 217, 1, '2017-08-15 07:28:24', '2017-08-15 07:28:24'),
(226, 217, 217, 1, '2017-08-15 07:28:25', '2017-08-15 07:28:25'),
(227, 217, 217, 1, '2017-08-15 07:28:27', '2017-08-15 07:28:27'),
(228, 217, 217, 1, '2017-08-15 07:28:28', '2017-08-15 07:28:28'),
(229, 217, 217, 1, '2017-08-15 07:28:30', '2017-08-15 07:28:30'),
(230, 217, 217, 1, '2017-08-15 07:28:31', '2017-08-15 07:28:31'),
(231, 216, 216, 1, '2017-08-15 07:28:35', '2017-08-15 07:28:35'),
(232, 216, 216, 1, '2017-08-15 07:28:36', '2017-08-15 07:28:36'),
(233, 216, 216, 1, '2017-08-15 07:28:37', '2017-08-15 07:28:37'),
(234, 216, 216, 1, '2017-08-15 07:28:37', '2017-08-15 07:28:37'),
(235, 216, 216, 1, '2017-08-15 07:28:39', '2017-08-15 07:28:39'),
(236, 216, 216, 1, '2017-08-15 07:29:00', '2017-08-15 07:29:00'),
(237, 216, 216, 1, '2017-08-15 07:29:01', '2017-08-15 07:29:01'),
(238, 216, 216, 1, '2017-08-15 07:29:02', '2017-08-15 07:29:02'),
(239, 216, 216, 1, '2017-08-15 07:29:02', '2017-08-15 07:29:02'),
(240, 216, 216, 1, '2017-08-15 07:29:03', '2017-08-15 07:29:03'),
(241, 216, 216, 1, '2017-08-15 07:29:03', '2017-08-15 07:29:03'),
(242, 216, 216, 1, '2017-08-15 07:29:04', '2017-08-15 07:29:04'),
(243, 216, 216, 1, '2017-08-15 07:29:05', '2017-08-15 07:29:05'),
(244, 216, 216, 1, '2017-08-15 07:29:06', '2017-08-15 07:29:06'),
(245, 216, 216, 1, '2017-08-15 07:29:07', '2017-08-15 07:29:07'),
(246, 216, 216, 1, '2017-08-15 07:29:07', '2017-08-15 07:29:07'),
(247, 216, 216, 1, '2017-08-15 07:29:08', '2017-08-15 07:29:08'),
(248, 216, 216, 1, '2017-08-15 07:29:08', '2017-08-15 07:29:08'),
(249, 216, 216, 1, '2017-08-15 07:29:10', '2017-08-15 07:29:10'),
(250, 216, 216, 1, '2017-08-15 07:29:11', '2017-08-15 07:29:11'),
(251, 216, 216, 1, '2017-08-15 07:29:11', '2017-08-15 07:29:11'),
(252, 216, 216, 1, '2017-08-15 07:29:12', '2017-08-15 07:29:12'),
(253, 216, 216, 1, '2017-08-15 07:29:13', '2017-08-15 07:29:13'),
(254, 216, 216, 1, '2017-08-15 07:29:14', '2017-08-15 07:29:14'),
(255, 216, 216, 1, '2017-08-15 07:29:14', '2017-08-15 07:29:14'),
(256, 216, 216, 1, '2017-08-15 07:29:15', '2017-08-15 07:29:15'),
(257, 216, 216, 1, '2017-08-15 07:29:16', '2017-08-15 07:29:16'),
(258, 217, 217, 1, '2017-08-15 07:29:19', '2017-08-15 07:29:19'),
(259, 217, 217, 1, '2017-08-15 07:29:20', '2017-08-15 07:29:20'),
(260, 217, 217, 1, '2017-08-15 07:29:25', '2017-08-15 07:29:25'),
(261, 217, 217, 1, '2017-08-15 07:29:25', '2017-08-15 07:29:25'),
(262, 217, 217, 1, '2017-08-15 07:29:25', '2017-08-15 07:29:25'),
(263, 217, 217, 1, '2017-08-15 07:29:27', '2017-08-15 07:29:27'),
(264, 217, 217, 1, '2017-08-15 07:29:30', '2017-08-15 07:29:30'),
(265, 217, 217, 1, '2017-08-15 07:29:32', '2017-08-15 07:29:32'),
(266, 217, 217, 1, '2017-08-15 07:29:33', '2017-08-15 07:29:33'),
(267, 217, 217, 1, '2017-08-15 07:29:33', '2017-08-15 07:29:33'),
(268, 217, 217, 1, '2017-08-15 07:29:34', '2017-08-15 07:29:34'),
(269, 217, 217, 1, '2017-08-15 07:29:35', '2017-08-15 07:29:35'),
(270, 217, 217, 1, '2017-08-15 07:29:36', '2017-08-15 07:29:36'),
(271, 217, 217, 1, '2017-08-15 07:29:37', '2017-08-15 07:29:37'),
(272, 217, 217, 1, '2017-08-15 07:29:37', '2017-08-15 07:29:37'),
(273, 217, 217, 1, '2017-08-15 07:29:39', '2017-08-15 07:29:39'),
(274, 217, 217, 1, '2017-08-15 07:29:40', '2017-08-15 07:29:40'),
(275, 217, 217, 1, '2017-08-15 07:29:41', '2017-08-15 07:29:41'),
(276, 217, 217, 1, '2017-08-15 07:29:42', '2017-08-15 07:29:42'),
(277, 217, 217, 1, '2017-08-15 07:29:42', '2017-08-15 07:29:42'),
(278, 217, 217, 1, '2017-08-15 07:29:49', '2017-08-15 07:29:49'),
(279, 217, 217, 1, '2017-08-15 07:29:50', '2017-08-15 07:29:50'),
(280, 217, 217, 1, '2017-08-15 07:29:52', '2017-08-15 07:29:52'),
(281, 217, 217, 1, '2017-08-15 07:29:53', '2017-08-15 07:29:53'),
(282, 217, 217, 1, '2017-08-15 07:29:54', '2017-08-15 07:29:54'),
(283, 217, 217, 1, '2017-08-15 07:29:55', '2017-08-15 07:29:55'),
(284, 217, 217, 1, '2017-08-15 07:29:57', '2017-08-15 07:29:57'),
(285, 217, 217, 1, '2017-08-15 07:29:58', '2017-08-15 07:29:58'),
(286, 217, 217, 1, '2017-08-15 07:29:59', '2017-08-15 07:29:59'),
(287, 217, 217, 1, '2017-08-15 07:30:00', '2017-08-15 07:30:00'),
(288, 217, 217, 1, '2017-08-15 07:30:01', '2017-08-15 07:30:01'),
(289, 188, 188, 1, '2017-09-13 03:25:36', '2017-09-13 03:25:36'),
(290, 204, 204, 1, '2017-09-13 03:25:38', '2017-09-13 03:25:38'),
(291, 205, 205, 1, '2017-09-13 03:25:39', '2017-09-13 03:25:39'),
(292, 206, 206, 1, '2017-09-13 03:25:41', '2017-09-13 03:25:41'),
(293, 206, 206, 1, '2017-09-13 03:25:41', '2017-09-13 03:25:41'),
(294, 206, 206, 1, '2017-09-13 03:25:42', '2017-09-13 03:25:42'),
(295, 206, 206, 1, '2017-09-13 03:25:42', '2017-09-13 03:25:42'),
(296, 204, 204, 2, '2017-09-13 03:25:48', '2017-09-13 03:25:48'),
(297, 188, 188, 2, '2017-09-13 03:25:49', '2017-09-13 03:25:49'),
(298, 204, 204, 2, '2017-09-13 03:25:50', '2017-09-13 03:25:50'),
(299, 189, 189, 2, '2018-01-03 06:24:13', '2018-01-03 06:24:13'),
(300, 188, 188, 2, '2018-01-03 06:24:18', '2018-01-03 06:24:18'),
(301, 204, 204, 2, '2018-01-03 06:24:19', '2018-01-03 06:24:19'),
(302, 225, 225, 2, '2018-01-03 06:24:25', '2018-01-03 06:24:25'),
(303, 189, 189, 2, '2018-01-03 06:24:27', '2018-01-03 06:24:27'),
(304, 188, 188, 2, '2018-01-03 06:24:59', '2018-01-03 06:24:59'),
(305, 204, 204, 2, '2018-01-29 09:33:25', '2018-01-29 09:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `marc_subfield_structure`
--

CREATE TABLE `marc_subfield_structure` (
  `sub_id` int(10) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `tagsubfield` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `tagsubfieldname` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `repeatable` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marc_subfield_structure`
--

INSERT INTO `marc_subfield_structure` (`sub_id`, `id`, `tagsubfield`, `tagsubfieldname`, `repeatable`, `created_at`, `updated_at`) VALUES
(22, 14, 'a', 'international standard book number', 'none', '2017-06-28 05:21:05', '2017-06-28 05:21:05'),
(23, 14, 'c', 'terms of availability', 'none', '2017-06-28 05:21:23', '2017-06-28 05:21:23'),
(24, 14, 'z', 'cancelled/invalid ISBN', 'r', '2017-06-28 05:21:41', '2017-06-28 05:21:41'),
(25, 15, 'a', 'personal name', 'none', '2017-06-28 05:38:15', '2017-06-28 05:38:15'),
(26, 15, 'b', 'numeration', 'none', '2017-06-28 05:41:05', '2017-06-28 05:41:05'),
(27, 15, 'c', 'titles and other words associated with a name', 'r', '2017-06-28 05:44:04', '2017-06-28 05:44:04'),
(28, 15, 'q', 'fuller form of a name', 'none', '2017-06-28 05:44:31', '2017-06-28 05:44:31'),
(29, 15, 'd', 'dates associated with a name', 'none', '2017-06-28 05:46:44', '2017-06-28 05:46:44'),
(30, 16, 'a', 'title proper', 'none', '2017-06-28 05:49:52', '2017-06-28 05:49:52'),
(31, 16, 'p', 'name of part/section of a work', 'r', '2017-06-28 05:50:48', '2017-06-28 05:50:48'),
(32, 16, 'b', 'remainder of title', 'none', '2017-06-28 05:51:22', '2017-06-28 05:51:22'),
(33, 16, 'c', 'remainder of title page/statement of responsibility', 'none', '2017-06-28 05:51:47', '2017-06-28 05:51:47'),
(34, 17, 'a', 'edition statement', 'none', '2017-06-28 05:57:52', '2017-06-28 05:57:52'),
(35, 18, 'a', 'place of publication, distribution, etc.', 'r', '2017-06-28 06:06:56', '2017-06-28 06:06:56'),
(36, 18, 'b', 'name of publisher , distributor, etc.', 'r', '2017-06-28 06:07:22', '2017-06-28 06:07:22'),
(37, 18, 'c', 'date of publication, distribution, etc.', 'r', '2017-06-28 06:08:28', '2017-06-28 06:08:28'),
(38, 19, 'a', 'extent (number of pages)', 'none', '2017-06-28 06:12:02', '2017-06-28 06:12:02'),
(39, 19, 'b', 'other physical details', 'none', '2017-06-28 06:12:24', '2017-06-28 06:12:24'),
(40, 19, 'c', 'dimensions (cm)', 'r', '2017-06-28 06:12:37', '2017-06-28 06:12:37'),
(41, 19, 'e', 'accompanying material', 'none', '2017-06-28 06:12:52', '2017-06-28 06:12:52'),
(42, 20, 'a', 'series statement', 'r', '2017-06-28 06:19:18', '2017-06-28 06:19:18'),
(43, 20, 'v', 'volume number', 'r', '2017-06-28 06:19:31', '2017-06-28 06:19:31'),
(44, 21, 'a', 'ORIGINAL CATALOGING AGENCY', 'none', '2018-04-12 02:40:42', '2018-04-12 02:40:42'),
(45, 21, 'c', 'TRANSCRIBING AGENCY', 'none', '2018-04-12 02:41:20', '2018-04-12 02:41:20'),
(46, 21, 'D', 'MODIFYING AGENCY', 'r', '2018-04-12 02:41:55', '2018-04-12 02:41:55'),
(47, 22, 'a', 'MEETING/CONFERENCE OR JURISDICTION NAME', 'none', '2018-04-12 02:44:25', '2018-04-12 02:44:25'),
(48, 22, 'n', 'NUMBER OF MEETING', 'none', '2018-04-12 02:44:42', '2018-04-12 02:44:42'),
(49, 22, 'd', 'DATE OF MEETING', 'none', '2018-04-12 02:44:58', '2018-04-12 02:44:58'),
(50, 22, 'c', 'LOCATION OF MEETING /CONFERENCE', 'none', '2018-04-12 02:45:23', '2018-04-12 02:45:23'),
(51, 23, 'a', 'UNIFORM TITLE', 'none', '2018-04-12 02:46:43', '2018-04-12 02:46:43'),
(52, 23, 'p', 'NAME OF PART / SECTION OF WORK', 'r', '2018-04-12 02:47:11', '2018-04-12 02:47:11'),
(53, 23, 'l', 'LANGUAGE OF WORK', 'none', '2018-04-12 02:47:29', '2018-04-12 02:47:29'),
(54, 23, 's', 'VERSION', 'none', '2018-04-12 02:47:44', '2018-04-12 02:47:44'),
(55, 23, 'f', 'DATE OF A WORK', 'none', '2018-04-12 02:48:03', '2018-04-12 02:48:03'),
(56, 24, 'a', 'UNIFORM TITLE', 'none', '2018-04-12 02:50:18', '2018-04-12 02:50:18'),
(57, 24, 'l', 'LANGUAGE OF WORK', 'none', '2018-04-12 02:50:36', '2018-04-12 02:50:36'),
(58, 24, 'f', 'DATE OF A WORK', 'none', '2018-04-12 02:50:55', '2018-04-12 02:50:55'),
(59, 25, 'a', 'TITLE PROPER', 'none', '2018-04-12 02:52:39', '2018-04-12 02:52:39'),
(60, 26, 'a', 'CONTENT TYPE', 'none', '2018-04-12 02:53:47', '2018-04-12 02:53:47'),
(61, 26, '2', 'RDA CONTENT', 'none', '2018-04-12 02:54:34', '2018-04-12 02:54:34'),
(62, 27, 'a', 'MEDIA TYPE', 'none', '2018-04-12 02:55:14', '2018-04-12 02:55:14'),
(63, 27, '2', 'RDA MEDIA', 'none', '2018-04-12 02:55:26', '2018-04-12 02:55:26'),
(64, 28, 'a', 'CARRIER TYPE', 'none', '2018-04-12 02:56:11', '2018-04-12 02:56:11'),
(65, 28, '2', 'RDA CARRIER', 'none', '2018-04-12 02:56:29', '2018-04-12 02:56:29'),
(66, 29, 'a', 'TITLE', 'none', '2018-04-12 03:00:19', '2018-04-12 03:00:19'),
(67, 29, 'v', 'VOLUME NUMBER', 'none', '2018-04-12 03:00:38', '2018-04-12 03:00:38'),
(68, 30, 'a', 'GENERAL NOTE IS USED WHEN NO SPECIAL SPECIALIZED NOTE FIELD HAS BEEN DEFINED FOR THE INFORMATION', 'none', '2018-04-12 03:03:17', '2018-04-12 03:03:17'),
(69, 31, 'a', '\"WITH\" NOTE', 'none', '2018-04-12 05:18:19', '2018-04-12 05:18:19'),
(70, 32, 'a', 'BIBLIOGRAPHY , ETC. NOTE', 'none', '2018-04-12 05:19:13', '2018-04-12 05:19:13'),
(71, 33, 'a', 'FORMATTED CONTENTS NOTE', 'none', '2018-04-12 05:20:21', '2018-04-12 05:20:21'),
(72, 34, 'a', 'SUMMARY , ABSTRACT , OR ANNOTATION', 'none', '2018-04-12 05:22:08', '2018-04-12 05:22:08'),
(73, 34, 'B', 'EXPANSION OF SUMMARY NOTE', 'none', '2018-04-12 05:22:30', '2018-04-12 05:22:30'),
(74, 35, 'a', 'PERTAINS TO THE AGE LEVEL AT WHICH THE ITEM WILL MOST LIKELY BE OF INTEREST', 'none', '2018-04-12 05:23:51', '2018-04-12 05:23:51'),
(75, 36, 'P', 'INTRODUCTORY PHRASE', 'none', '2018-04-12 05:24:57', '2018-04-12 05:24:57'),
(76, 36, 'C', 'PUBLICATION OF THE ORIGINAL', 'none', '2018-04-12 05:25:13', '2018-04-12 05:25:13'),
(77, 36, 'T', 'TITLE OF THE ORIGINAL', 'none', '2018-04-12 05:25:25', '2018-04-12 05:25:25'),
(78, 37, 'A', 'PERSONAL NAME (SURNAME AND FORENAME)', 'none', '2018-06-13 06:40:45', '2018-06-13 06:40:45'),
(79, 37, 'B', 'NUMERATION', 'none', '2018-04-12 05:26:43', '2018-04-12 05:26:43'),
(80, 37, 'C', 'TITLE AND OTHER WORDS ASSOCIATE WITH THE NAME', 'r', '2018-04-12 05:27:02', '2018-04-12 05:27:02'),
(81, 37, 'Q', 'FULLER FORM OF NAME', 'none', '2018-04-12 05:27:24', '2018-04-12 05:27:24'),
(82, 37, 'D', 'DATES ASSOCIATE WITH A NAME (GENERALLY, YEAR OF BIRTH)', 'none', '2018-04-12 05:28:04', '2018-04-12 05:28:04'),
(83, 37, 'T', 'TITLE OF WORK', 'none', '2018-04-12 05:28:18', '2018-04-12 05:28:18'),
(84, 37, 'V', 'FORM SUBDIVISION', 'r', '2018-04-12 05:28:35', '2018-04-12 05:28:35'),
(85, 37, 'X', 'GENERAL SUBDIVISION', 'r', '2018-04-12 05:35:59', '2018-04-12 05:35:59'),
(86, 37, 'Y', 'CHRONOLOGICAL SUBDIVISION', 'r', '2018-04-12 05:39:28', '2018-04-12 05:39:28'),
(87, 37, 'z', 'GEOGRAPHIC SUBDIVISION', 'r', '2018-04-12 05:39:58', '2018-04-12 05:39:58'),
(88, 37, '2', 'SOURCE OF HEADING OR ITEM (USED WITH SECOND INDICATOR OF 7)', 'none', '2018-04-12 05:40:34', '2018-04-12 05:40:34'),
(89, 38, 'A', 'CORPORATE NAME OR JURISDICTION NAME AS ENTRY ELEMENT', 'none', '2018-04-12 05:43:56', '2018-04-12 05:43:56'),
(90, 38, 'B', 'SUBORDINATE UNIT', 'r', '2018-04-12 05:44:27', '2018-04-12 05:44:27'),
(91, 38, 'V', 'FORM SUBDIVISION', 'r', '2018-04-12 05:45:23', '2018-04-12 05:45:23'),
(92, 38, 'X', 'GENERAL SUBDIVISION', 'r', '2018-04-12 05:45:36', '2018-04-12 05:45:36'),
(93, 38, 'Y', 'CHRONOLOGICAL SUBDIVISION', 'r', '2018-04-12 05:45:52', '2018-04-12 05:45:52'),
(94, 38, 'Z', 'GEOGRAPHICAL SUBDIVISION R', 'r', '2018-04-12 05:46:14', '2018-04-12 05:46:14'),
(95, 38, '2', 'SOURCE OF HEADING OR TERM (USED WITH 2ND INDICATOR OF 7)', 'none', '2018-04-12 05:46:42', '2018-04-12 05:46:42'),
(96, 39, 'A', 'TOPICAL TERM', 'none', '2018-04-12 05:48:48', '2018-04-12 05:48:48'),
(97, 39, 'V', 'FORM SUBDIVISION', 'r', '2018-04-12 05:49:02', '2018-04-12 05:49:02'),
(98, 39, 'X', 'GENERAL SUBDIVISION', 'r', '2018-04-12 05:49:39', '2018-04-12 05:49:39'),
(99, 39, 'Y', 'CHRONOLOGICAL SUBDIVISION', 'r', '2018-04-12 05:50:00', '2018-04-12 05:50:00'),
(100, 39, 'Z', 'GEOGRAPHIC SUBDIVISION', 'r', '2018-04-12 05:50:45', '2018-04-12 05:50:45'),
(101, 39, '2', 'SOURCE OF HEADING OR TERM USED WITH 2ND INDICATOR OF 7', 'none', '2018-04-12 05:51:39', '2018-04-12 05:51:39'),
(102, 40, 'A', 'GEOGRAPHIC NAME', 'none', '2018-04-12 05:52:47', '2018-04-12 05:52:47'),
(103, 40, 'V', 'FORM SUBDIVISION', 'r', '2018-04-12 05:53:06', '2018-04-12 05:53:06'),
(104, 40, 'X', 'GENERAL SUBDIVISION', 'r', '2018-04-12 05:53:16', '2018-04-12 05:53:16'),
(105, 40, 'Y', 'CHRONOLOGICAL SUBDIVISION', 'r', '2018-04-12 05:53:37', '2018-04-12 05:53:37'),
(106, 40, 'Z', 'GEOGRAPHIC SUBDIVISION', 'r', '2018-04-12 05:53:51', '2018-04-12 05:53:51'),
(107, 40, '2', 'SOURCE OF HEADING OR TERM (USED WITH 2ND INDICATOR OF 7)', 'none', '2018-04-12 05:54:16', '2018-04-12 05:54:16'),
(108, 41, 'A', 'PERSONAL NAME', 'none', '2018-04-12 05:56:13', '2018-04-12 05:56:13'),
(109, 41, 'b', 'NUMERATION', 'none', '2018-04-12 05:56:43', '2018-04-12 05:56:43'),
(110, 41, 'C', 'TITLES AND OTHER WORDS ASSOCIATED WITH A NAME', 'r', '2018-04-12 05:57:21', '2018-04-12 05:57:21'),
(111, 41, 'Q', 'FULLER FORM OF NAME', 'none', '2018-04-12 05:57:56', '2018-04-12 05:57:56'),
(112, 41, 'd', 'DATES ASSOCIATED WITH A NAME (GENERALLY, YEAR OF BIRTH)', 'none', '2018-04-12 05:59:29', '2018-04-12 05:59:29'),
(113, 41, 'E', 'RELATOR TERM (SUCH AS ILLUSTRATOR)', 'r', '2018-04-12 06:00:48', '2018-04-12 06:00:48'),
(114, 41, '4', 'RELATOR', 'r', '2018-04-12 06:01:14', '2018-04-12 06:01:14'),
(115, 42, 'A', 'CORPORATE NAME OR JURISDICTION NAME AS ENTRY ELEMENT', 'none', '2018-04-12 06:03:06', '2018-04-12 06:03:06'),
(116, 42, 'B', 'SUBORDINATE UNIT', 'r', '2018-04-12 06:03:24', '2018-04-12 06:03:24'),
(117, 43, 'A', 'PERSONAL NAME', 'none', '2018-04-12 06:04:10', '2018-04-12 06:04:10'),
(118, 43, 'B', 'NUMERATION', 'none', '2018-04-12 06:04:20', '2018-04-12 06:04:20'),
(119, 43, 'C', 'TITLES AND OTHER WORDS ASSOCIATED WITH A NAME', 'r', '2018-04-12 06:04:39', '2018-04-12 06:04:39'),
(120, 43, 'Q', 'FULLER FORM OF NAME', 'none', '2018-04-12 06:04:58', '2018-04-12 06:04:58'),
(121, 43, 'D', 'DATES ASSOCIATED WITH A NAME  (GENERALLY , YEAR OF BIRTH)', 'none', '2018-04-12 06:05:31', '2018-04-12 06:05:31'),
(122, 43, 'T', 'TITLE OF A WORK', 'none', '2018-04-12 06:05:47', '2018-04-12 06:05:47'),
(123, 43, 'V', 'VOLUME NUMBER', 'none', '2018-04-12 06:06:00', '2018-04-12 06:06:00'),
(124, 44, 'a', 'Classification number', 'r', '2018-05-10 02:05:35', '2018-05-10 02:05:35'),
(125, 44, 'b', 'item number', 'nr', '2018-05-10 02:05:53', '2018-05-10 02:05:53');

-- --------------------------------------------------------

--
-- Table structure for table `marc_tag_structure`
--

CREATE TABLE `marc_tag_structure` (
  `id` int(10) UNSIGNED NOT NULL,
  `tagfield` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `repeatable` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marc_tag_structure`
--

INSERT INTO `marc_tag_structure` (`id`, `tagfield`, `tagname`, `repeatable`, `record_type`, `created_at`, `updated_at`) VALUES
(14, '020', 'International standard book number', 'none', 'bibliographic', '2017-06-28 05:20:34', '2017-06-28 05:20:34'),
(15, '100', 'MAIN ENTRY - PERSONAL NAME (PRIMARY AUTHOR)', 'nr', 'bibliographic', '2017-06-28 05:37:43', '2018-04-12 02:49:28'),
(16, '245', 'title statement', 'nr', 'bibliographic', '2017-06-28 05:47:19', '2017-06-28 05:47:19'),
(17, '250', 'edition statement', 'nr', 'bibliographic', '2017-06-28 05:57:28', '2017-06-28 05:57:28'),
(18, '264', 'publication, distribution, etc.', 'r', 'bibliographic', '2017-06-28 05:59:17', '2017-06-28 05:59:17'),
(19, '300', 'physical description', 'r', 'bibliographic', '2017-06-28 06:11:38', '2017-06-28 06:11:38'),
(20, '490', 'SERIES STATEMENT - NO ADD ENTRY IS TRACED FROM FIELD', 'r', 'bibliographic', '2017-06-28 06:18:55', '2018-04-12 03:01:02'),
(21, '040', 'CATALOGING SOURCE', 'nr', 'bibliographic', '2018-04-12 02:39:44', '2018-04-12 02:39:44'),
(22, '111', 'MAIN ENTRY - CONFERENCES AND OTHER MEETINGS (PRIMARY AUTHOR)', 'none', 'bibliographic', '2018-04-12 02:43:43', '2018-04-12 02:43:43'),
(23, '130', 'MAIN ENTRY - UNIFORM TITLE', 'nr', 'bibliographic', '2018-04-12 02:46:14', '2018-04-12 02:46:14'),
(24, '240', 'UNIFORM TITLE', 'nr', 'bibliographic', '2018-04-12 02:48:40', '2018-04-12 02:48:40'),
(25, '246', 'VERIFYING FORM OF TITLE', 'r', 'bibliographic', '2018-04-12 02:52:17', '2018-04-12 02:52:17'),
(26, '336', 'CONTENT TYPE', 'none', 'bibliographic', '2018-04-12 02:53:21', '2018-04-12 02:53:21'),
(27, '337', 'MEDIA TYPE', 'none', 'bibliographic', '2018-04-12 02:54:54', '2018-04-12 02:54:54'),
(28, '338', 'CARRIER TYPE', 'none', 'bibliographic', '2018-04-12 02:55:51', '2018-04-12 02:55:51'),
(29, '440', 'SERIES STATEMENT / ADD ENTRY - TITLE', 'r', 'bibliographic', '2018-04-12 02:59:54', '2018-04-12 02:59:54'),
(30, '500', 'GENERAL NOTE', 'r', 'bibliographic', '2018-04-12 03:02:14', '2018-04-12 03:02:14'),
(31, '501', '\"WITH\" NOTE', 'none', 'bibliographic', '2018-04-12 05:17:45', '2018-04-12 05:17:45'),
(32, '504', 'BIBLIOGRAPHY , ETC . NOTE', 'r', 'bibliographic', '2018-04-12 05:18:44', '2018-04-12 05:18:44'),
(33, '505', 'FORMATTED CONTENTS NOTE', 'r', 'bibliographic', '2018-04-12 05:19:56', '2018-04-12 05:19:56'),
(34, '520', 'SUMMARY ETC. NOTE', 'r', 'bibliographic', '2018-04-12 05:21:23', '2018-04-12 05:21:23'),
(35, '521', 'TARGET AUDIENCE NOTE', 'none', 'bibliographic', '2018-04-12 05:23:05', '2018-04-12 05:23:05'),
(36, '534', 'ORIGINAL VERSION NOTE', 'none', 'bibliographic', '2018-04-12 05:24:21', '2018-04-12 05:24:21'),
(37, '600', 'SUBJECT ADD ENTRY  (PERSONAL NAME)', 'r', 'bibliographic', '2018-04-12 05:25:57', '2018-04-12 05:25:57'),
(38, '610', 'SUBJECT ADD ENTRY - CORPORATE NAME', 'r', 'bibliographic', '2018-04-12 05:42:59', '2018-04-12 05:42:59'),
(39, '650', 'SUBJECT ADDED ENTRY - TROPICAL TERM', 'r', 'bibliographic', '2018-04-12 05:47:58', '2018-04-12 05:47:58'),
(40, '651', 'SUBJECT ADDED ENTRY - GEOGRAPHIC NAME', 'r', 'bibliographic', '2018-04-12 05:51:56', '2018-04-12 05:52:17'),
(41, '700', 'ADDED ENTRY - PERSONAL NAME', 'r', 'bibliographic', '2018-04-12 05:55:28', '2018-04-12 05:55:28'),
(42, '710', 'ADDED ENTRY - CORPORATE NAME', 'r', 'bibliographic', '2018-04-12 06:02:02', '2018-04-12 06:02:11'),
(43, '800', 'SERIES ADDED ENTRY - PERSONAL NAME', 'r', 'bibliographic', '2018-04-12 06:03:49', '2018-04-12 06:03:49'),
(44, '050', 'Call Number', 'r', 'bibliographic', '2018-05-10 02:05:05', '2018-05-10 02:05:05');

-- --------------------------------------------------------

--
-- Table structure for table `marc_tag_structure_cat_templates`
--

CREATE TABLE `marc_tag_structure_cat_templates` (
  `id` int(11) UNSIGNED DEFAULT NULL,
  `template_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marc_tag_structure_cat_templates`
--

INSERT INTO `marc_tag_structure_cat_templates` (`id`, `template_id`) VALUES
(14, 8),
(15, 8),
(16, 8),
(17, 8),
(18, 8),
(19, 8),
(20, 8),
(15, 10),
(14, 9),
(15, 9),
(16, 9),
(17, 9),
(18, 9),
(19, 9),
(20, 9),
(21, 9),
(22, 9),
(23, 9),
(24, 9),
(25, 9),
(26, 9),
(27, 9),
(28, 9),
(29, 9),
(30, 9),
(31, 9),
(32, 9),
(33, 9),
(34, 9),
(35, 9),
(36, 9),
(37, 9),
(38, 9),
(39, 9),
(40, 9),
(41, 9),
(42, 9),
(43, 9);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2017_05_08_000001_status', 1),
(37, '2017_05_08_000002_auth_user', 1),
(38, '2017_05_08_000003_auth_permission', 1),
(39, '2017_05_08_000004_auth_role', 1),
(40, '2017_05_08_000005_auth_role_permission', 1),
(41, '2017_05_08_000006_auth_user_role', 1),
(42, '2017_05_08_000007_user_profile', 1),
(43, '2017_05_08_000008_marc_tag_structure', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patrons`
--

CREATE TABLE `patrons` (
  `patron_id` int(10) NOT NULL,
  `barcode` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `sis_id` int(10) DEFAULT NULL,
  `card_number` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `patron_type` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `patron_category_id` int(10) UNSIGNED DEFAULT NULL,
  `status_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `expiration` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patrons`
--

INSERT INTO `patrons` (`patron_id`, `barcode`, `sis_id`, `card_number`, `patron_type`, `patron_category_id`, `status_id`, `created_at`, `updated_at`, `expiration`) VALUES
(188, '1', 0, '', 'student', 1, 1, '2017-07-17 03:37:59', '2017-09-26 02:13:18', '2017-05-22 09:51:10'),
(189, '123', 0, '', 'student', 1, 1, '2017-07-17 03:37:59', '2018-04-16 02:28:59', '2018-10-29 10:28:59'),
(190, 'S12345670', 0, '', 'student', 1, 1, '2017-07-10 06:08:17', '2017-09-26 02:13:19', '2017-10-29 13:54:17'),
(204, '2', 0, '', 'faculty', 2, 1, '2017-07-17 03:38:00', '2018-01-16 05:09:25', '2018-10-29 13:09:25'),
(205, '3', 0, '', 'faculty', 2, 1, '2017-07-17 03:38:01', '2017-11-08 06:53:14', '2018-03-29 14:53:14'),
(206, '4', 0, '', 'faculty', 2, 1, '2017-07-17 03:38:01', '2017-09-26 02:13:37', '2017-10-29 15:08:51'),
(212, '455', 1, '', 'student', 1, 1, '2017-07-17 03:38:02', '2017-09-26 02:13:19', '2017-10-29 08:25:02'),
(213, '556', 4, '', 'student', 1, 1, '2017-07-13 00:29:15', '2017-11-02 05:34:41', '2018-03-29 13:34:41'),
(214, '222', 3, '', 'student', 1, 1, '2017-07-17 03:38:03', '2017-09-26 02:13:19', '2017-10-29 08:26:32'),
(215, '900', 2, '', 'student', 1, 1, '2017-07-18 00:50:09', '2017-09-26 02:13:19', '2017-10-29 08:26:34'),
(216, '78', 0, '', 'student', 1, 1, '2017-08-07 00:57:22', '2017-09-26 02:13:19', '2017-10-29 08:57:22'),
(217, 'S12345678', 621, '', 'student', 1, 1, '2017-08-15 07:17:26', '2017-09-26 02:13:19', '2017-10-29 15:17:26'),
(218, '00', 700, '', 'student', 1, 1, '2017-08-15 07:17:28', '2017-09-26 02:13:19', '2017-10-29 15:17:28'),
(219, 'S12345679', 615, '', 'student', 1, 1, '2017-08-15 07:17:31', '2017-09-26 02:13:19', '2017-10-29 15:17:31'),
(220, '89', 671, '', 'student', 1, 1, '2017-08-15 07:17:33', '2017-09-26 02:13:19', '2017-10-29 15:17:33'),
(221, '999', 682, '', 'student', 1, 1, '2017-08-15 07:17:37', '2017-09-26 02:13:19', '2017-10-29 15:17:37'),
(222, '90', 623, '', 'student', 1, 1, '2017-08-15 07:17:40', '2017-09-26 02:13:19', '2017-10-29 15:17:40'),
(223, '98', 664, '', 'student', 1, 1, '2017-08-15 07:17:44', '2017-09-26 02:13:20', '2017-10-29 15:17:44'),
(224, '345', 670, '', 'student', 1, 1, '2017-08-15 07:17:46', '2017-09-26 02:13:20', '2017-10-29 15:17:46'),
(225, '456', 643, '', 'student', 1, 1, '2017-08-15 07:17:48', '2017-09-26 02:13:20', '2017-10-29 09:46:54'),
(226, '777', 701, '', 'student', 1, 1, '2017-08-15 07:17:53', '2017-09-26 02:13:20', '2017-10-29 15:17:53'),
(227, '43534', 653, '', 'student', 1, 1, '2017-08-15 07:17:54', '2017-09-26 02:13:20', '2017-10-29 15:17:54'),
(228, '5464', 706, '', 'student', 1, 1, '2017-08-15 07:17:56', '2017-09-26 02:13:20', '2017-10-29 15:17:56'),
(229, '66', 0, '', 'faculty', 2, 1, '2017-09-06 08:58:46', '2017-09-26 02:13:41', '2017-10-29 16:58:46'),
(230, '111111111', 0, '', 'student', 1, 1, '2017-09-11 02:11:04', '2017-09-26 02:13:20', '2017-10-29 10:11:04'),
(231, '8888888888', 0, '', 'student', 1, 1, '2017-09-12 06:25:52', '2017-09-26 02:13:20', '2017-10-29 14:25:52'),
(232, '55', 0, '', 'student', 1, 1, '2018-12-02 02:26:23', '2018-02-05 05:53:08', '2017-10-29 10:26:23'),
(233, '123456', 0, '', 'student', 1, 1, '2018-05-09 02:51:54', '2018-05-09 02:52:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patron_categories`
--

CREATE TABLE `patron_categories` (
  `patron_category_id` int(10) UNSIGNED NOT NULL,
  `category_type_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `enrolment_period` int(10) DEFAULT NULL,
  `enrolment_period_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patron_categories`
--

INSERT INTO `patron_categories` (`patron_category_id`, `category_type_id`, `description`, `enrolment_period`, `enrolment_period_date`) VALUES
(1, 5, '3', 3, NULL),
(2, 6, 'sample', NULL, '2017-09-30'),
(5, 7, 'sdad', 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patron_category_type`
--

CREATE TABLE `patron_category_type` (
  `category_type_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patron_category_type`
--

INSERT INTO `patron_category_type` (`category_type_id`, `category_name`, `description`) VALUES
(5, 'student', 'a person enrolled'),
(6, 'staff', 'test'),
(7, 'librarian', 'sample'),
(8, 'adult', 'sample');

-- --------------------------------------------------------

--
-- Table structure for table `patron_information`
--

CREATE TABLE `patron_information` (
  `patron_id` int(10) NOT NULL,
  `full_name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `student_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `course` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `address` text CHARACTER SET latin1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patron_information`
--

INSERT INTO `patron_information` (`patron_id`, `full_name`, `gender`, `student_id`, `dob`, `course`, `contact_no`, `address`, `created_at`, `updated_at`) VALUES
(188, 'ZED ROB MED', 'Male', '12-5459-29298', '1994-02-12', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', '09129487473', 'P5 A Silad Mahogany Butuan City', '2017-07-10 05:48:12', '2017-07-10 05:48:28'),
(189, 'Toomy Cruizz', 'Male', '12-302-11', '1994-02-01', 'Home Economics', '091282843', 'P6CM Silad M', '2017-07-10 05:52:38', '2017-11-02 03:18:08'),
(190, 'asd asdads asdasd', 'Male', '0029-1234-66', '1994-04-02', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', '1023912312', 'Maguindanao', '2017-07-10 05:54:17', '2017-07-10 05:54:17'),
(204, 'Miguel asd Mal', 'Male', '12-019-23452', '1987-02-12', 'FACULTY', '09128274672', '123', '2017-07-11 06:55:41', '2017-09-06 08:59:11'),
(205, 'Mime TER MA', 'Male', '12-01-33-2212', '2002-02-27', 'FACULTY', '0192883212', 'asd213123', '2017-07-11 07:07:15', '2017-09-06 08:59:10'),
(206, 'miking asd asd', 'female', '12-04848-28383', '1993-01-22', 'FACULTY', '012939213', 'asdasdasd', '2017-07-11 07:08:51', '2017-09-06 08:59:08'),
(212, 'RACHEL GALDO CURILAN', 'female', '13001998000', '2017-06-06', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-07-13 00:25:02', '2017-07-13 00:25:02'),
(213, 'CHRISTIAN IVY B OLIVO', 'female', '13002405700', '1992-06-27', 'BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-07-13 00:26:30', '2017-09-04 00:59:28'),
(214, 'RUSTOM RAMOS PEDALES', 'male', '12-01-33-0013', '1995-02-12', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-07-13 00:26:32', '2017-09-11 06:04:36'),
(215, 'SHIELA FLOR DALAPO CABALAN', 'female', '1003016300', '2017-06-06', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-07-13 00:26:35', '2017-07-13 00:26:35'),
(216, 'BRADLY ASD A', 'Male', '123-222-0000', '1995-08-07', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', '09128274627', 'T_BURGOS MAHGOAN', '2017-08-07 00:57:22', '2017-08-07 00:58:07'),
(217, 'DEWITT ROOB KOSS', 'male', '73784107', '1995-08-04', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:26', '2017-09-04 00:59:47'),
(218, 'EMILE BEIER BEER', 'male', '84816072', '1943-10-13', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:28', '2017-08-15 07:17:28'),
(219, 'DYLAN WEIMANN LABADIE', 'male', '3896226', '1922-06-02', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:31', '2017-08-15 07:17:31'),
(220, 'IRMA LUEILWITZ MOORE', 'female', '2317339', '1985-08-29', 'INFORMATION AND COMMUNICATIONS TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:34', '2017-08-15 07:17:34'),
(221, 'FRANCESCA RUNTE QUIGLEY', 'female', '37568468', '1985-08-29', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:38', '2017-08-15 07:17:38'),
(222, 'LARON DONNELLY CUMMERATA', 'male', '49335045', '1978-05-28', 'ACCOUNTANCY, BUSINESS AND MANAGEMENT', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:40', '2017-08-15 07:17:40'),
(223, 'KALE MCCULLOUGH STOLTENBERG', 'male', '19875880', '1985-08-29', 'INDUSTRIAL ARTS', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:44', '2017-08-15 07:17:44'),
(224, 'MARIETTA MURAZIK KSHLERIN', 'female', '47965450', '1985-08-29', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:46', '2017-08-15 07:17:46'),
(225, 'MATEO CHRISTIANSEN HARRIS', 'male', '32841163', '1985-08-29', 'BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:48', '2017-08-15 07:17:48'),
(226, 'SALMA JACOBSON O\'HARA', 'female', '34950301', '1985-08-29', 'COMPUTER-BASED ACCOUNTING', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:53', '2017-08-15 07:17:53'),
(227, 'TANYA ZBONCAK CROOKS', 'female', '43026739', '1985-08-29', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:54', '2017-08-15 07:17:54'),
(228, 'TITUS FEIL PROSACCO', 'male', '86304795', '1985-08-29', 'INFORMATION AND COMMUNICATIONS TECHNOLOGY', NULL, 'MAHAY BUTUAN CITY AGUSAN DEL NORTE PHILIPPINES', '2017-08-15 07:17:56', '2017-08-15 07:17:56'),
(229, 'MEPO CARE MAN', 'Male', '12-302-11', '1983-02-23', 'FACULTY', '09128273646', 'PS PWEDE BA', '2017-09-06 08:58:46', '2017-09-06 08:59:38'),
(230, 'jyde Kapunan Lacuesta', 'Male', '77-77-77-7777', '1995-06-15', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', '09951291190', 'P-2 Antongalon', '2017-09-11 02:11:04', '2017-09-11 08:44:05'),
(231, 'you know who', 'Male', '88-88-88-8888', '1995-06-15', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', '99999999999', 'at', '2017-09-12 06:25:52', '2017-09-12 06:25:52'),
(232, 'tim ariliano saw', 'Male', '123-33-23-2-2', '1997-02-12', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', '0909123034', 'asdad', '2017-09-26 02:26:24', '2017-09-26 02:26:24'),
(233, 'Mot Mot2 Mot', 'Male', '89088908', '1994-12-21', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', '091282824322', 'PED BUTUAN CITY', '2018-05-09 02:51:54', '2018-05-09 02:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` int(11) NOT NULL,
  `c_id` int(11) DEFAULT NULL,
  `p_name` varchar(200) DEFAULT NULL,
  `p_creator` varchar(200) DEFAULT NULL,
  `p_description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `c_id`, `p_name`, `p_creator`, `p_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Vivlio V.1', 'Engtech Global Solutions', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-08-15 02:54:40', '2017-08-17 02:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `renewal`
--

CREATE TABLE `renewal` (
  `r_id` int(10) UNSIGNED NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `l_id` int(11) UNSIGNED DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserves`
--

CREATE TABLE `reserves` (
  `reserve_id` int(10) NOT NULL,
  `patron_id` int(10) DEFAULT NULL,
  `reserved_date` datetime DEFAULT NULL,
  `catalogue_record_id` int(10) UNSIGNED DEFAULT NULL,
  `copy_id` int(10) UNSIGNED DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `remarks` text,
  `priority` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reserves`
--

INSERT INTO `reserves` (`reserve_id`, `patron_id`, `reserved_date`, `catalogue_record_id`, `copy_id`, `cancel_date`, `remarks`, `priority`, `status`, `created_at`, `updated_at`) VALUES
(1, 204, '2017-10-24 14:13:48', 16, NULL, '2017-11-09 10:22:20', NULL, NULL, 'canceled', '2017-11-09 02:20:38', '2017-11-22 01:04:32'),
(2, 190, '2017-10-24 14:59:24', 17, NULL, '2017-11-09 14:21:41', NULL, NULL, 'canceled', '2017-11-09 02:20:47', '2017-11-22 01:04:37'),
(23, 189, '2017-11-21 00:00:00', 18, 2, NULL, ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n											 	 	 	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n											 	 	 	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', NULL, 'borrowed', '2017-11-21 01:32:45', '2018-04-16 02:29:31'),
(24, 189, '2017-11-21 00:00:00', 17, 5, NULL, ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n											 	 	 	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n											 	 	 	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', NULL, 'borrowed', '2017-11-21 06:35:49', '2018-05-09 02:50:39'),
(25, 205, '2018-01-16 00:00:00', 18, NULL, NULL, 'asss', NULL, 'pending', '2018-01-16 04:58:15', '2018-01-16 04:58:15'),
(26, 204, '2018-04-16 00:00:00', 18, NULL, NULL, 'aasd', NULL, 'pending', '2018-04-16 02:27:03', '2018-04-16 02:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `label`, `description`, `created_at`, `updated_at`) VALUES
(1, 'active', 'active', '2017-05-11 01:16:22', '2017-05-15 01:16:25'),
(2, 'inactive', 'inactive', '2017-05-03 01:16:28', '2017-05-31 01:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `full_name`, `gender`, `position`, `contact_no`) VALUES
(1, 'Tom Ramos Pedales', 'male', 'Developer', '09128274646');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `ao_id` (`ao_id`);

--
-- Indexes for table `announcement_option`
--
ALTER TABLE `announcement_option`
  ADD PRIMARY KEY (`ao_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`perm_id`);

--
-- Indexes for table `auth_role`
--
ALTER TABLE `auth_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `auth_role_permission`
--
ALTER TABLE `auth_role_permission`
  ADD KEY `auth_role_permission_role_id_foreign` (`role_id`),
  ADD KEY `auth_role_permission_perm_id_foreign` (`perm_id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `auth_user_status_id_foreign` (`status_id`);

--
-- Indexes for table `auth_user_role`
--
ALTER TABLE `auth_user_role`
  ADD KEY `auth_user_role_role_id_foreign` (`role_id`),
  ADD KEY `auth_user_role_user_id_foreign` (`user_id`);

--
-- Indexes for table `catalogue_record`
--
ALTER TABLE `catalogue_record`
  ADD PRIMARY KEY (`catalogue_id`),
  ADD KEY `material` (`material_type_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `cat_templates`
--
ALTER TABLE `cat_templates`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `copies`
--
ALTER TABLE `copies`
  ADD PRIMARY KEY (`copy_id`),
  ADD UNIQUE KEY `accession_num` (`acc_num`),
  ADD UNIQUE KEY `barcode` (`barcode`) USING BTREE,
  ADD KEY `copies_idfk_ctr` (`catalogue_id`);

--
-- Indexes for table `expiration_history`
--
ALTER TABLE `expiration_history`
  ADD PRIMARY KEY (`expiration_id`),
  ADD KEY `patron_id` (`patron_id`) USING BTREE;

--
-- Indexes for table `e_resources`
--
ALTER TABLE `e_resources`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `field_value`
--
ALTER TABLE `field_value`
  ADD PRIMARY KEY (`field_id`),
  ADD KEY `field_value_idfk_marc_id` (`id`),
  ADD KEY `field_value_idfk_catalogue_id` (`catalogue_id`);

--
-- Indexes for table `fines`
--
ALTER TABLE `fines`
  ADD PRIMARY KEY (`f_id`),
  ADD KEY `patron_id` (`patron_id`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Indexes for table `lib_material_type`
--
ALTER TABLE `lib_material_type`
  ADD PRIMARY KEY (`material_type_id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `loans_idfk_patrons_patron_id` (`patron_id`);

--
-- Indexes for table `loan_rules`
--
ALTER TABLE `loan_rules`
  ADD PRIMARY KEY (`patron_category_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `logs_ibfk_2` (`patron_id`);

--
-- Indexes for table `marc_subfield_structure`
--
ALTER TABLE `marc_subfield_structure`
  ADD PRIMARY KEY (`sub_id`),
  ADD KEY `mss_idfk_mts_id` (`id`);

--
-- Indexes for table `marc_tag_structure`
--
ALTER TABLE `marc_tag_structure`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tagfield` (`tagfield`) USING BTREE;

--
-- Indexes for table `marc_tag_structure_cat_templates`
--
ALTER TABLE `marc_tag_structure_cat_templates`
  ADD KEY `id` (`id`),
  ADD KEY `template_id` (`template_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patrons`
--
ALTER TABLE `patrons`
  ADD PRIMARY KEY (`patron_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `patron_idfk_pc` (`patron_category_id`);

--
-- Indexes for table `patron_categories`
--
ALTER TABLE `patron_categories`
  ADD PRIMARY KEY (`patron_category_id`),
  ADD UNIQUE KEY `category_type_id` (`category_type_id`) USING BTREE;

--
-- Indexes for table `patron_category_type`
--
ALTER TABLE `patron_category_type`
  ADD PRIMARY KEY (`category_type_id`);

--
-- Indexes for table `patron_information`
--
ALTER TABLE `patron_information`
  ADD KEY `patron_information_idfk_patrons_id` (`patron_id`) USING BTREE;

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `c_id` (`c_id`);

--
-- Indexes for table `renewal`
--
ALTER TABLE `renewal`
  ADD PRIMARY KEY (`r_id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `l_id` (`l_id`);

--
-- Indexes for table `reserves`
--
ALTER TABLE `reserves`
  ADD PRIMARY KEY (`reserve_id`),
  ADD KEY `patron_id` (`patron_id`),
  ADD KEY `catalogue_record_id` (`catalogue_record_id`),
  ADD KEY `copy_id` (`copy_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD KEY `user_profile_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `announcement_option`
--
ALTER TABLE `announcement_option`
  MODIFY `ao_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `perm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_role`
--
ALTER TABLE `auth_role`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `catalogue_record`
--
ALTER TABLE `catalogue_record`
  MODIFY `catalogue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cat_templates`
--
ALTER TABLE `cat_templates`
  MODIFY `template_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `copies`
--
ALTER TABLE `copies`
  MODIFY `copy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `expiration_history`
--
ALTER TABLE `expiration_history`
  MODIFY `expiration_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `e_resources`
--
ALTER TABLE `e_resources`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `field_value`
--
ALTER TABLE `field_value`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `fines`
--
ALTER TABLE `fines`
  MODIFY `f_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `lib_material_type`
--
ALTER TABLE `lib_material_type`
  MODIFY `material_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `loan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=306;

--
-- AUTO_INCREMENT for table `marc_subfield_structure`
--
ALTER TABLE `marc_subfield_structure`
  MODIFY `sub_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `marc_tag_structure`
--
ALTER TABLE `marc_tag_structure`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `patrons`
--
ALTER TABLE `patrons`
  MODIFY `patron_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `patron_categories`
--
ALTER TABLE `patron_categories`
  MODIFY `patron_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patron_category_type`
--
ALTER TABLE `patron_category_type`
  MODIFY `category_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `renewal`
--
ALTER TABLE `renewal`
  MODIFY `r_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reserves`
--
ALTER TABLE `reserves`
  MODIFY `reserve_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `announcement_ibfk_1` FOREIGN KEY (`ao_id`) REFERENCES `announcement_option` (`ao_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_role_permission`
--
ALTER TABLE `auth_role_permission`
  ADD CONSTRAINT `auth_role_permission_perm_id_foreign` FOREIGN KEY (`perm_id`) REFERENCES `auth_permission` (`perm_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `auth_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD CONSTRAINT `auth_user_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

--
-- Constraints for table `auth_user_role`
--
ALTER TABLE `auth_user_role`
  ADD CONSTRAINT `auth_user_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `auth_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_user_role_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `catalogue_record`
--
ALTER TABLE `catalogue_record`
  ADD CONSTRAINT `material` FOREIGN KEY (`material_type_id`) REFERENCES `lib_material_type` (`material_type_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `copies`
--
ALTER TABLE `copies`
  ADD CONSTRAINT `copies_idfk_ctr` FOREIGN KEY (`catalogue_id`) REFERENCES `catalogue_record` (`catalogue_id`) ON UPDATE CASCADE;

--
-- Constraints for table `expiration_history`
--
ALTER TABLE `expiration_history`
  ADD CONSTRAINT `expiration_history_ibfk_1` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `field_value`
--
ALTER TABLE `field_value`
  ADD CONSTRAINT `field_value_idfk_catalogue_id` FOREIGN KEY (`catalogue_id`) REFERENCES `catalogue_record` (`catalogue_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `field_value_idfk_marc_id` FOREIGN KEY (`id`) REFERENCES `marc_tag_structure` (`id`);

--
-- Constraints for table `fines`
--
ALTER TABLE `fines`
  ADD CONSTRAINT `fines_ibfk_1` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`),
  ADD CONSTRAINT `fines_ibfk_2` FOREIGN KEY (`loan_id`) REFERENCES `loans` (`loan_id`);

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_idfk_patrons_patron_id` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `loan_rules`
--
ALTER TABLE `loan_rules`
  ADD CONSTRAINT `lr_idfk_patron_categories_pcid` FOREIGN KEY (`patron_category_id`) REFERENCES `patron_categories` (`patron_category_id`) ON UPDATE CASCADE;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `logs_ibfk_2` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `marc_subfield_structure`
--
ALTER TABLE `marc_subfield_structure`
  ADD CONSTRAINT `mss_idfk_mts_id` FOREIGN KEY (`id`) REFERENCES `marc_tag_structure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `marc_tag_structure_cat_templates`
--
ALTER TABLE `marc_tag_structure_cat_templates`
  ADD CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `marc_tag_structure` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `template_id` FOREIGN KEY (`template_id`) REFERENCES `cat_templates` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `patrons`
--
ALTER TABLE `patrons`
  ADD CONSTRAINT `patron_idfk_pc` FOREIGN KEY (`patron_category_id`) REFERENCES `patron_categories` (`patron_category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patrons_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `patron_categories`
--
ALTER TABLE `patron_categories`
  ADD CONSTRAINT `pc_idfk_pct_cti` FOREIGN KEY (`category_type_id`) REFERENCES `patron_category_type` (`category_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `patron_information`
--
ALTER TABLE `patron_information`
  ADD CONSTRAINT `patron_information_ibfk_1` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `company` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `renewal`
--
ALTER TABLE `renewal`
  ADD CONSTRAINT `renewal_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `patrons` (`patron_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `renewal_ibfk_2` FOREIGN KEY (`l_id`) REFERENCES `loans` (`loan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reserves`
--
ALTER TABLE `reserves`
  ADD CONSTRAINT `reserves_ibfk_1` FOREIGN KEY (`patron_id`) REFERENCES `patrons` (`patron_id`),
  ADD CONSTRAINT `reserves_ibfk_2` FOREIGN KEY (`catalogue_record_id`) REFERENCES `catalogue_record` (`catalogue_id`),
  ADD CONSTRAINT `reserves_ibfk_3` FOREIGN KEY (`copy_id`) REFERENCES `copies` (`copy_id`);

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
