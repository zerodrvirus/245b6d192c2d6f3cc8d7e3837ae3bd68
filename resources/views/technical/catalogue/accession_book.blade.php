<style type="text/css">
    
    #tbl-accession-book tr:hover{
        cursor: pointer;
        background-color: #f1f1f1;
    }

     #tbl-accession-book thead tr:first-child {
        background-color: #fff;
     }

    table.dataTable tbody>tr.selected, table.dataTable tbody>tr.selected td, table.dataTable tbody>tr>.selected {
        background-color: #f1f1f1!important;
    }

     tr.tableHeader > td {
        font-weight: bold;
    } 

</style>

<?php

    function single_out($field, $index) {

        $str = explode('_', $field);
        echo ucwords(substr($str[$index], 1, strlen($str[$index])));
    }

?>

<h3 class="m-t-10"> Accession Book </h3>
<p class="help-block">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

<div class="row">
    <div class="col-sm-12">
        <a href="#holdings-modal-dialog" data-toggle="modal" class="btn btn-primary btn-sm m-r-5 m-b-5 disabled" id="btn-add-copy"> <i class="fa fa-street-view"></i> View Copies</a>
        <button id="refresh-accession-book"><i class="fa fa-refresh"></i> Refresh </button> 
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table id="tbl-accession-book" class="display table-condensed table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr class="tableHeader">
                    <!-- <th>Accession Number</th> -->
                    <!-- <th>Date Received</th> -->
                    <td>Call Number</td>
                    <td>Author</td>
                    <td>Title</td>
                    <td>Edition</td>
                    <td>Volume</td>
                    <td>Pages</td>
                    <td>Price</td>
                    <td>Copies</td>
                    <td>Publishing House</td>
                    <td>Copyright Year</td>  
                    <td>ISBN</td>
                    <!-- <td>Remarks</td> -->
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($records as $key => $value): ?>
            		<tr id="<?php echo $value['catalogue_id']; ?>">
            			<td> <?php echo $value['call_num']; ?> </td>
            			<td> <?php single_out($value[0][1]->value, 1); ?> </td>
            			<td> <?php single_out($value[0][2]->value, 1); ?> </td>
            			<td> <?php single_out($value[0][3]->value, 1); ?> </td>
            			<td> <?php echo $value[0][6]->value; ?> </td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td> <?php single_out($value[0][4]->value, 2); ?> </td>
            			<td> <?php single_out($value[0][4]->value, 3); ?> </td>
            			<td> <?php single_out($value[0][0]->value, 1); ?> </td>
            			<!-- <td>  -->
                            <?php 
                                // echo substr($value['remarks'], 0, 30).(strlen(substr($value['remarks'], 0, 30)) > 29 ? '.. .' : ''); 
                            ?> 
                        <!-- </td> -->
            		</tr>
            	<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="holdings-modal-dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Holdings</h4>
            </div>
            <div class="modal-body">
               <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="col-sm-12" style="padding: 10px; border: 1px solid #CCC; background-color: #f1f1f1;">
                            
                            <form class="form-horizontal" id="frm_add_copy">
                                
                                <div class="col-sm-12 text-center">
                                    <h5>Copy Information - Create Single Item</h5>
                                    <input type = "hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" id="catalogue_id_copy" name="catalogue_id">
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Barcode <sup><i class="fa fa-asterisk text-danger"></i></sup></label>
                                        <div class="col-md-8">
                                            <input type="text" required class="form-control" name="barcode">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Accession No <sup><i class="fa fa-asterisk text-danger"></i></sup></label>
                                        <div class="col-md-8">
                                            <input type="text" required class="form-control" name="acc_num">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Source</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="source">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Call Number</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="call_num">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Material Type</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="add_copy_material_types" name="material_type_id"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Copy Number <sup><i class="fa fa-asterisk text-danger"></i></sup></label>
                                        <div class="col-md-8">
                                            <input type="number" required class="form-control" name="copy_number">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Price</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="price">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Copy Note</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="3" style="resize: none;" name="note"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Date Received</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="date" name="date_received" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 text-right">
                                   <button class="btn btn-sm btn-primary" id="btn_save_add_copy">Save</button>
                                </div>

                                
                            </form>
                            

                        </div>
                    </div>

                    <div class="col-sm-12" style="margin-top: 10px;"></div>

                    <div class="col-sm-12">

                        <table class="table table-condensed table-bordered" id="acc_tbl_copies">
                            <thead>
                                <tr>
                                    <td>Barcode</td>
                                    <td>Accession Number</td>
                                    <td>Call Number</td>
                                    <td>Copy Number</td>
                                    <td>Material Type</td>
                                    <td>Price</td>
                                    <td>Status</td>
                                    <td>Issues</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                        </table>

                    </div>

               </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                <a href="javascript:;" class="btn btn-sm btn-success">Action</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    $(function(){

        material_types();

        var tblAB = $('#tbl-accession-book').DataTable({

            "bPaginate": true,
            // "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            "pageLength": 100
        });

        var tblCopies = $('#acc_tbl_copies').DataTable();

        $('#tbl-accession-book tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
                $('#btn-add-copy').addClass('disabled');
            }
            else {
                tblAB.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                var idx = tblAB.cell('.selected', 0).index();
                var data = tblAB.row( idx.row ).data();

                get_copies(data.DT_RowId, tblCopies);

                $('#holdings-modal-dialog #catalogue_id_copy').val('');
                $('#holdings-modal-dialog #catalogue_id_copy').val(data.DT_RowId);
                $('#btn-add-copy').removeClass('disabled');
            }
        } );

        $(document).on('click', '#refresh-accession-book', function(e){
            e.preventDefault();
            
            call_accession_book($(this));

        });
        
        $(document).on('submit', '#frm_add_copy', function(e){
            e.preventDefault();

            var form = $(this);
           
            $.ajax({
                url     : "{{url('/technical/add_copy')}}", 
                type    : 'POST',
                data    : form.serialize(),
                error   : function(error){
                    $.gritter.add({
                            title:"<i class='fa fa-warning text-danger'></i> Internal Server Error [" + error.status + "]!",
                            text:"Failed to load resource or Duplicate entry",
                            sticky:false,
                            time:""
                    }); 
                    return false;
                },
                success : function(data){
                    // console.log(data)
                    if( data > 0 ){

                        $.gritter.add({
                            title:"<i class='fa fa-check text-success'></i> New Copy added!",
                            text:"",
                            sticky:false,
                            time:""
                        }); 

                    }else{

                          $.gritter.add({
                                title:"<i class='fa fa-warning text-danger'></i> Something went wrong",
                                text:"",
                                sticky:false,
                                time:""
                        }); 

                    }  
                    form[0].reset();
 
                },
            });
        });


    });

    function get_copies(id, table) {

        $.ajax({
                type: 'POST',   
                url: "{{url('/technical/get_copies')}}",
                data: {  _token: "{{csrf_token()}}", 'catalogue_id': id },
                success: function(data){

                    table.clear();
                    table.draw();
                    data.forEach(function(entry){

                        var actions = '<i class="fa fa-times text-danger"></i>';

                        table.row.add( [
                            entry.barcode,
                            entry.acc_num,
                            entry.call_num,
                            entry.copy_number,
                            entry.material_type_id,
                            entry.price,
                            entry.status,
                            entry.issues,
                            actions
                        ] ).draw( false );

                    });

                },
            });

    }

    /* fetch material types */
    function material_types(){

        $.ajax({
            type: 'POST',
            url: "{{url('/technical/fetch')}}",
            data: {  _token: "{{csrf_token()}}" },
            success: function(data){
                // console.log(data);
                var options = '';

                for (var key in data) {
                  if (data.hasOwnProperty(key)) {
                    // console.log(data[key].name);
                    options += '<option value="' + data[key].material_type_id + '">' + data[key].name.toUpperCase() + '</option>';
                  }
                }

                $('#add_copy_material_types').html(options);

            },
        });
    }1

</script>