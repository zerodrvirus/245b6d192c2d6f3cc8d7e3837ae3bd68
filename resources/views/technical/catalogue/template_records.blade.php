<style type="text/css">
	
	#tbl-full-marc-record tr:hover{
		cursor: pointer;
	}

	.selected {
		background-color: #e8e8e8;
	}

</style>
<?php //var_dump($data); ?>

<div style="height: 600px; overflow:scroll;">
	<table class="table table-condensed table-bordered" id="tbl-full-marc-record">
		<thead>
			<tr>
				<th width="250">Field Name</th>
				<th width="25">I1</th>
				<th width="25">I2</th>
				<th width="250">Subfield</th>
				<th width="450">Data</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $key => $value) : ?>
				
				<tr>
					<td>{{ ucwords($value['tagname']) }}</td><td></td><td></td><td></td><td></td>
				</tr>

				<?php foreach ($value['subfield'] as $k => $v) : ?>
					<tr class="items" sub_id="{{$v['sub_id']}}" subfield="{{$v['tagsubfield']}}" id="{{$value['id']}}" tagfield="{{ $value['tagfield'] }}" >
						<td></td><td></td><td></td><td>{{ ucwords($v['tagsubfieldname']) }}</td><td></td>
					</tr>
				<?php endforeach; ?>

			<?php endforeach; ?>
		</tbody>
	</table>
</div>