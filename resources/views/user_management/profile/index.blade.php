@extends('layouts.app')


@section('custom_css')
	<link href="{{asset('public/_color/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" /> 
	<link href="{{asset('public/_color/custom.css')}}" rel="stylesheet" />
	
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="{{asset('public/_color/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{asset('public/_color/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	<style type="text/css">
		.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover{
			color: white!important;
			background: rgba(0,64,64,0.8)!important;
		}
		.nav-pills>li>a{
			background: #fff!important;
			color: gray;
		}
		.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
			background: #004040 !important;
			color: white !important;
		}
		.form-control input-sm{
			width: 80%!important;
		}
	</style>
@endsection

@section('content')
<!-- begin #content -->
<div id="content" class="content">
	<div class="row">
		<div class="col-md-6 col-sm-12">
			 <div class="panel panel-default">
			 	 <div class="panel-heading">
			 	 	 <span style="color:#004040;"><b><i class="fa fa-users"></i> Employee List </b></span>
			 	 </div>
			 	 <div class="panel-body">
			 	 	<table id="user_list" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="color:004040!important;">ID#</th>
                                <th style="color:004040!important;">USER</th>
                                <th style="color:004040!important;">POSITION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>11</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                             <tr>
                                <td>1</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>Don Del Rosario</td>
                                <td>Manager</td>
                            </tr>

                        </tbody>
                    </table>
			 	 </div>
			 </div>
		</div>
		<div class="col-md-6 col-sm-12">
			 <div class="panel panel-default">
			 	 <div class="panel-heading">
			 	 	 <span style="color:#004040;"><i class="fa fa-user"></i> User Account Access Portal</span>
			 	 </div>
			 	 <div class="panel-body">
			 	 	 <div class="media media-sm">
						<a class="media-left" href="javascript:;">
							<img src="{{asset('public/images/user-1.fw.png')}}" alt="" class="media-object">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Del Rosario</h4>
							<span>Maria Riza</span>
							<span class="pull-right"><a class="btn btn-flat btn-xs btn-success"> <i class="fa fa-pencil"></i> Update</a> <a class="btn btn-flat btn-xs btn-success"><i class="fa fa-stop"></i> Deactivate</a> <a class="btn btn-flat btn-xs btn-success"> <i class="fa fa-times"></i> Remove</a></span>
							<p style="color:#c5931c;" >0123X213</p>
						</div>
						<div class="form-group" style="height:10px;"></div>
						<div class="row">
							<div id="jstree-checkable"></div>
						</div>
					</div>
			 	 </div>
			 </div>
		</div>		
	</div>
</div>
@endsection

@section('custom_js')
	
	<script src="{{asset('public/_color/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('public/_color/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('public/_color/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('public/_color/plugins/jstree/dist/jstree.min.js')}}"></script>

	
	
	<script type="text/javascript">
		$(document).ready(function(){
			var table = $('#user_list');
			table.DataTable();
			$("#jstree-checkable").jstree({
			plugins:["wholerow","checkbox","types"],
			core:{themes:{responsive:!1},
			data:[{text:"Same but with checkboxes",
			children:[{text:"initially selected",
			state:{
				selected:!0}},
				{text:"Folder 1"},
				{text:"Folder 2"},
				{text:"Folder 3"},
				{text:"initially open",
					icon:"fa fa-folder fa-lg",
					state:{opened:!0},
					children:[
						{text:"Another node"},
						{text:"disabled node",state:{disabled:!0}}
						]
				},
				{text:"custom icon",icon:"fa fa-cloud-download fa-lg text-inverse"},
				{text:"disabled node",state:{disabled:!0}}]},"Root node 2"]},
				types:{"default":{icon:"fa fa-folder text-primary fa-lg"},
				file:{icon:"fa fa-file text-success fa-lg"}
			}})
		});

	</script>

@endsection