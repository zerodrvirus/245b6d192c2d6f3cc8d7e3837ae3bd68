<html>
<head>
    <title>{{$data['title']}}</title>
    <link rel="stylesheet" type="text/css" href="{{asset('public/bootstrap/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/dist/css/templates/login.css')}}">
    <link href="{{asset('public/_color/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('public/images/vivlio_388.fw.png')}}">
     <link href="{{asset('public/_color/custom.css')}}" rel="stylesheet" />
</head>
<style type="text/css">
    a{
        color:#004040;
    }
	a:hover,a:focus{
        text-decoration:none;
        color :#004040;
    }
    .text-red{
        color: #B20000;
    }
	.form-control:focus, btn:focus {
		border-color:#ffffff;
		-webkit-box-shadow: none;
		box-shadow: none;
    }
    .btn-primary,btn{
        color: #fff!important;
        background-color: #004040 !important;
        border-color: #004040 !important;
    }
    .itm_container{
        background:#fff;
        padding:20px;
        margin-bottom:10px; 
    }

</style>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-success">
        <a href="{{url('opac')}}" class="navbar-brand">
                <img class="img img-responsive" src="{{url('public/images/logo.fw.png')}}" style="margin:auto;vertical-align:middle;margin-top:-4px;margin-left:0px;">
        </a>
         <div class="collapse navbar-collapse" id="" style="margin-top:9px;">
            <form class="form-inline col-md-11 my-lg-0">
                <input class="form-control col-md-12" style="width:40%;" type="text" placeholder="Search Keyword, Title etc.">
                <input class="form-control col-md-12" style="width:15%; margin-left:12px;" type="text" placeholder="Topics">
                <input class="form-control col-md-12" style="width:15%; margin-left:12px;" type="text" placeholder="Author">
                <button class="btn col-md-2 btn-flat btn-primary" style="margin-left:12px;">Search</button>
            </form>
        </div>
    </nav>
    <div class="col-md-12">
        <div class="col-md-9">
           <div class="row">
               <p style="color:gray;">About 2,560,000 results (0.58 seconds) </p>
                <div class="col-md-12 itm_container">
                    <p style="float:left;">
                        <img src="{{url('public/images/book-default.fw.png')}}" style="height:120px;" alt="items">
                    </p>
                    <p style="color:#004040;"><h4><a href="#">Software Engineering and Design</a></h4></p>
                    <p style="font-size:0.8em; color:#004040;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font> <font style="margin-left:30px;">Call No: 456234</font></p>
                    <p style="font-size:1em; text-style:justify;">Almost every day, Google introduces changes to its ranking algorithm. Some are tiny tweaks; others seriously shake up the SERPs. This cheat sheet will help you make sense of the most important algo changes and penalties rolled out in the recent years, with a brief overview and SEO advice on each.</p>
                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                </div>
                <div class="col-md-12 itm_container">
                    <p style="float:left;">
                        <img src="{{url('public/images/book-default.fw.png')}}" style="height:120px;" alt="items">
                    </p>
                    <p style="color:#004040;"><h4><a href="#">Software Engineering and Design</a></h4></p>
                    <p style="font-size:0.8em; color:#004040;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font> <font style="margin-left:30px;">Call No: 456234</font></p>
                    <p style="font-size:1em; text-style:justify;">Almost every day, Google introduces changes to its ranking algorithm. Some are tiny tweaks; others seriously shake up the SERPs. This cheat sheet will help you make sense of the most important algo changes and penalties rolled out in the recent years, with a brief overview and SEO advice on each.</p>
                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                </div>
                <div class="col-md-12 itm_container">
                    <p style="float:left;">
                        <img src="{{url('public/images/book-default.fw.png')}}" style="height:120px;" alt="items">
                    </p>
                    <p style="color:#004040;"><h4><a href="#">Software Engineering and Design</a></h4></p>
                    <p style="font-size:0.8em; color:#004040;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font> <font style="margin-left:30px;">Call No: 456234</font></p>
                    <p style="font-size:1em; text-style:justify;">Almost every day, Google introduces changes to its ranking algorithm. Some are tiny tweaks; others seriously shake up the SERPs. This cheat sheet will help you make sense of the most important algo changes and penalties rolled out in the recent years, with a brief overview and SEO advice on each.</p>
                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                </div>
                <div class="col-md-12 itm_container">
                    <p style="float:left;">
                        <img src="{{url('public/images/book-default.fw.png')}}" style="height:120px;" alt="items">
                    </p>
                    <p style="color:#004040;"><h4><a href="#">Software Engineering and Design</a></h4></p>
                    <p style="font-size:0.8em; color:#004040;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font> <font style="margin-left:30px;">Call No: 456234</font></p>
                    <p style="font-size:1em; text-style:justify;">Almost every day, Google introduces changes to its ranking algorithm. Some are tiny tweaks; others seriously shake up the SERPs. This cheat sheet will help you make sense of the most important algo changes and penalties rolled out in the recent years, with a brief overview and SEO advice on each.</p>
                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                </div>
           </div>
        </div>
        <div class="col-md-3">
            @if(!$data['is_login'])
                <div class="col-md-12" style="padding:10px;background:#eeeeee;">
                    <h4>Login To Your Account</h4>
                    <input class="form-control" id="auth_input" type="text" placeholder="ACCOUNT NUMBER" style="text-align:center;">
                    <div class="col-md-12" style="height:20px;"></div>
                    <span><small>Please authenticate OPAC (<font style="color:#004040;">Online Public Access Catalogue</font>) using your student ID </small></span>
                </div>
            @else 
                <div class="col-md-12" style="padding:10px;background:#eeeeee;">
                 <h4 style="color:#004040;">Library Records</h4>
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                             Borrows
                            </a>
                        </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <small style="color:#004040;">Notifications</small>
                            <div class="row">
                                 <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Reserves
                            </a>
                        </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                          <small style="color:#004040;">Notifications</small>
                            <div class="row">
                                 <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Collapsible Group Item #3
                            </a>
                        </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                          <small style="color:#004040;">Notifications</small>
                            <div class="row">
                                 <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                                <div class="col-md-12 itm_container">
                                    <p style="color:#004040;"><h5>Software Engineering and Design</h5></p>
                                    <p style="font-size:0.8em; color:#B20000;">3rd Edition <font style="margin-left:30px;">Accession No: 300919239</font></p>
                                    <p style="font-size:0.8em; text-style:justify;">omnis voluptate eius. Temporibus nostrum  eius. Temporibus nostrum eius. Temporibus nostrum</p>
                                    <button class="btn btn-xs btn-primary pull-right">Reserve</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
             </div>
            @endif 
             
             <div class="col-md-12" style="height:20px;"></div>
             
        </div>
    </div>
</body>

<script src="{{asset('public/_color/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>



<script type="text/javascript">
    $(document).ready(function(e){
           $(document).on('change','#auth_input',function(e){
                e.preventDefault();
                $(this).val('');
            });
    });
</script>
</html>