<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogueRecord extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'catalogue_record';

  	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'catalogue_id';
    public $timestamps = true;
    protected $fillable = [
            'material_type_id', 'call_num', 'remarks', 'price',
    ];


    public function add($hasValue = false , $array = array()) {
    	
        if( ! $hasValue ){
            $this->save();
            return $this->catalogue_id;
        }

    	$this->material_type_id = $request['material_type_id'];
		$this->call_num = $request['call_num'];
		$this->remarks = $request['remarks'];
		$this->price = $request['price'];
		$this->save();

		return $this->catalogue_id;
    }

    public function addKeyValuePairs($array){

        if( count($array) > 0 ){

            foreach ($array as $key => $value) { 
                $this->{$value['name']} = $value['value'];
            }

            $this->save();
            return $this->catalogue_id;
        }

        return false;
    }

    public function get_record_by_id($catalogue_id) {

        return $this->where('catalogue_id', $catalogue_id)->first()->toArray();

    }


}
