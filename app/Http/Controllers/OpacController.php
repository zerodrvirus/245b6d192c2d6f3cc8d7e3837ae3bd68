<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class OpacController extends Controller
{
    public $is_loggin = false;
    public function __construct(Request $request)
	{
       
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = ['title'=> 'Vivlio | '.ucfirst("OPAC"), 'name'=> 'Online Public Access Catalog', 'category'=>'Online Public Access Catalog','is_login' => $request->session()->has('studentSession')];
        return view('opac.index', compact('data'));
    }
    public function loadItemsList(Request $request){
        $data = ['title'=> 'Vivlio | '.ucfirst("OPAC"), 'name'=> 'Online Public Access Catalog', 'category'=>'Online Public Access Catalog','is_login' => $request->session()->has('studentSession')];
        return view('opac.itemListView', compact('data'));
    }
   
    
}
