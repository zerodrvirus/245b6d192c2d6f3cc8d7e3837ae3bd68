<?php

namespace App\Http\Controllers\technical;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LibMaterialType;
use App\CatalogueRecord;
use App\marc_tag_structure;
use App\cat_templates;
use App\marc_tag_structure_cat_templates;
use App\FieldValue;
use App\Copies;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CatalogController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
    public function fetch(Request $request) {

    	$lib_material_type = new LibMaterialType();
    	return $lib_material_type::all();	
    }
    public function store(Request $request) {
    	//validate request
        //check for errors
        /* -------------------------- */
        $templateVals = DB::table('cat_templates') 
				->join('marc_tag_structure_cat_templates', 'marc_tag_structure_cat_templates.template_id', '=',  'cat_templates.template_id')
				->join('marc_tag_structure', 'marc_tag_structure_cat_templates.id', '=' , 'marc_tag_structure.id')
				->groupBy('marc_tag_structure.tagfield')
				->get(['marc_tag_structure.tagfield', 'marc_tag_structure.tagname', 'marc_tag_structure_cat_templates.id']);
        /* -------------------------- */
    	// main record. without marc tags.
        $catalogue_record = new CatalogueRecord();
        $marcObj = new marc_tag_structure();
        /* -------------------------- */
        // fill in attributes in obj - insert record should follow.
    	$catalogue_id = $catalogue_record->add($request->input());
    	/* -------------------------- */
    	foreach ($templateVals as $key => $value) {
    		$value->value = null;
    		$marcObj->fix_tag_value($value->id, $catalogue_id, $request->input($value->tagfield));
    		// break;
    	}
    	/* -------------------------- */
    	return $catalogue_id;
    }
    public function add_copy(Request $request){

        $copy = new Copies();

        // return $request->input();
        $copy->acc_num = $request->input('acc_num');
        $copy->catalogue_id = $request->input('catalogue_id');
        $copy->barcode = $request->input('barcode');
        $copy->source = $request->input('source');
        $copy->status = 'available';
        $copy->note = $request->input('note');
        $copy->call_num = $request->input('call_num');
        $copy->issues = $request->input('issues');
        $copy->date_received = $request->input('date_received');
        $copy->price = $request->input('price');
        $copy->material_type_id = $request->input('material_type_id');
        $copy->copy_number = $request->input('copy_number');
        

        if ( $result = $copy->save() ){
            return 1;
        }

        return 0;
        
    }
    public function get_copies(Request $request){

        $catalogue_id = $request->input('catalogue_id');
        
        $copy = new Copies();
        $copies = $copy->where('catalogue_id', $catalogue_id)->get()->toArray();
        return $copies;
    }
    public function populate(){
        /* -------------------------- */
    	$records = DB::table('catalogue_record')
    				->join('lib_material_type', 'catalogue_record.material_type_id', '=', 'lib_material_type.material_type_id')
    				->get(['lib_material_type.name as material_name',
							'catalogue_record.catalogue_id',
							'catalogue_record.material_type_id',
							'catalogue_record.call_num',
							'catalogue_record.remarks',
							'catalogue_record.price'])->toArray();
        /*-------------------------- */
        $marc = new marc_tag_structure();
        $basic = $marc::whereIn('tagfield', ['245', '100', '250', '264'])->get(['id'])->toArray();
        // $title = $marc::where('tagfield', '245')->get(['id'])->first();
        // 245 - title statement, 100 - main entry, 250 - edition statement
        foreach ($records as $key => $value) {
            $fieldValue = new FieldValue();
            // fetch ---- so to speak.. .
            $fields = $fieldValue::where('catalogue_id', $value->catalogue_id)
                                    ->whereIn('id', $basic)
                                    ->get(['id', 'field_id', 'value'])
                                    ->toArray();

            $arr = array( 'author', 'title', 'edition', 'year' );

            # sequence - author, title, edition, publisher
            foreach ($fields as $k => $v) {
                    
                $val = explode('_', $fields[$k]['value']);
                array_splice($val, 0, 1);

                $substr = function($el) {
                    return strtoupper(substr($el, 1, strlen($el)));
                };

                if( $arr[$k] == 'year' ){
                    $newVal = array_map($substr, $val)[2];
                    $value->{$arr[$k]} = $newVal;
                    continue;
                }

                $newVal = array_map($substr, $val)[0];
                $value->{$arr[$k]} = $newVal;
            }
        }
      
        /* -------------------------- */
    	return response()->json(['data' => $records]); 
        // return $records;
    }

    public function accession_book(){
        /* -------------------------- */
      
        $fv           = new FieldValue();
        $cr           = new CatalogueRecord();
        $ids          = CatalogueRecord::orderBy('catalogue_id', 'ASC')->pluck('catalogue_id')->toArray();

        $records = array();

        foreach ($ids as $id) {
            
            $records[$id] = $cr->get_record_by_id($id);
            $records[$id][] = $fv->accession_by_id($id);

            // echo '<pre>';
            //     var_export($records);
            // echo '</pre>';

            // break;

        }

        // $array        = array('call_number', 'author', 'title', 'edition', 'volume');
        // $records = array();
        
        // if( count($ids) ) {
            
        //     foreach ($ids as $id) {
                
                // $records[$id] = $fv->accession_by_id($id);
                // echo '<pre>';
                // var_export($fv->accessionByCatalogueId($id));
                // echo '</pre>';
                // break;
        //     }
        // }
        // $records            = $fv->accession();
        
        // return response()->json(['data' => $records]);
        // dd($records);
        return view('technical.catalogue.accession_book', compact(['records']));

    }

    public function view_catalog_record(Request $request){

        $record = DB::table('catalogue_record')
                    ->join('lib_material_type', 'catalogue_record.material_type_id', '=', 'lib_material_type.material_type_id')
                    ->where('catalogue_record.catalogue_id', $request->input('id'))
                    ->get(['lib_material_type.name as material_name',
                            'catalogue_record.catalogue_id',
                            'catalogue_record.material_type_id',
                            'catalogue_record.call_num',
                            'catalogue_record.remarks',
                            'catalogue_record.price'])->toArray();

        $lib_material_type = new LibMaterialType();
        $material_types = response()->json($lib_material_type::all()->toArray());

        $records = $this->get_record($request->input('id'));
        $data = [$record, $records, $material_types];
        // $catalogue_record = $this->get_record($request->input('id'));
        // $array = array();
        // $data = [$catalogue_record, $array];
        return view('technical.catalogue.view_catalog', compact('data'));
    }
    private function get_record($id){

        # basic cataloging
        # id - 14, 15, 16, 17, 18, 19, 20
        # tagfield 020, 100, 245, 250, 264, 300, 490
        // $catalogue_record = new CatalogueRecord();
        $marc = new marc_tag_structure(); 
        $basic = $marc::whereIn('tagfield', ['020', '100', '245', '250', '264', '300', '490'])->get(['id'])->toArray();

        $array = array('isbn', 'main_entry', 'title_statement', 'edition_statement', 'publication', 'physical_description', 'series_statement');
        $records = array();

        $arrID = array();
        foreach ($basic as $key => $value) {
            $arrID[$key] = $value['id'];
        }

        $fieldValue = new FieldValue();
        $fields = $fieldValue::where('catalogue_id', $id)
                                    ->whereIn('id', $arrID)
                                    ->get(['id', 'field_id', 'value'])
                                    ->toArray();

        foreach ($fields as $k => $v) {
            
            $val = explode('_', $fields[$k]['value']);
         
            $substr = function($el) {
                $fieldValue = strtoupper(substr($el, 1, strlen($el)));
                $subfield = substr($el, 0, 1);
                $return = array($subfield, $fieldValue);
                return $return;
            };

            $newVal = array_map($substr, $val);
            foreach ($newVal as $k1 => $v1) {
                $records[$array[$k]][$v1[0]] = $v1[1];
            }
          
        }
        return $records;
    }
    public function get_record_by_isbn(Request $request){

        $isbn = $request->input('isbn');

        $marc = new marc_tag_structure();
        $fv = new FieldValue();

        $id = $marc->get_id_by_tag_field('020');
        # check if isbn is null;
        $values = $fv->get_isbn($id, $isbn);

        if( $values ){

            $catalogue_id = $values['catalogue_id'];
            var_dump($fv->retrieve($catalogue_id));
            // return $catalogue_id;
        }
        return 0;
    }

    public function add_marc_record(Request $request) {

        // $c_data = $request->input('form');
        $catalogue_record = new CatalogueRecord();
        // $catalogue_id = $catalogue_record->add($request->input());
        // $catalogue_id = $catalogue_record->addKeyValuePairs($c_data);
        
        // create catalogue_record.
        $catalogue_id = $catalogue_record->add(); 

        // return $catalogue_id;
        if( $catalogue_id > 0 ){

            $m_data = (array)json_decode($request->input('marc_records'));
            $arr = array();

            foreach ($m_data as $key => $value) { 
                $values = null;
                $records = explode('_', $value->records);
                $tag_id = $records[0];
                // $tagfield = $records[1];
                // $subfield_id = $records[2];
                $subfield = substr($records[count($records) - 1], 0, 1);
                $subfieldValue = substr($records[count($records) - 1], 1, strlen($records[count($records) - 1]));
                // $values .= '_'.$subfield.$subfieldValue;
                // $fieldObj->add($tag_id, $catalogue_id,)
                if( array_key_exists($tag_id, $arr) ){
                    $arr[$tag_id][] = '_'.$subfield.$subfieldValue;
                }else{
                    $arr[$tag_id][] = '_'.$subfield.$subfieldValue;
                }
            }

            foreach ($arr as $key => $value) {
                $fieldObj = new FieldValue();
                $fieldObj->add($key, $catalogue_id, implode('', $value));
            }
            return $catalogue_id;
        }
        return 0;
    }
}
